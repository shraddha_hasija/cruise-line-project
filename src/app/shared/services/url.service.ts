import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment";
declare var require: any;

@Injectable()
export class URLService {
  URLData: any = {};
  public BASE_URL: string = "http://10.12.40.192:8180/"; //STAGGING

  // public BASE_URL: string = "http://14.141.118.70:8010/adminpanel/"; //LIVE URL

  // public BASE_URL: string = "http://10.12.47.215:8080/"; local from darshan pc

  MOCK_BASE_URL: string = "/assets/mock-data/";

  constructor() {
  }

  getAPIData(serviceName: string) {

    switch (serviceName) {

      case 'createRestaurant':
        if (environment.MOCK_API)
          this.URLData = { URL: 'local.json', method: 'POST' }
        else
          this.URLData = { URL: 'api/restaurants', method: 'POST' }
        break;

      case 'getRestaurantList':
        if (environment.MOCK_API)
          this.URLData = { URL: 'local.json', method: 'GET' }
        else
          this.URLData = { URL: 'api/restaurants', method: 'GET' }
        break;

      case 'deleteDining':
        if (environment.MOCK_API)
          this.URLData = { URL: 'local.json', method: 'GET' }
        else
          this.URLData = { URL: 'api/restaurants/', method: 'DELETE' }
        break;
      case 'updateRestaurant':
        if (environment.MOCK_API)
          this.URLData = { URL: 'local.json', method: 'GET' }
        else
          this.URLData = { URL: 'api/restaurants', method: 'PUT' }
        break;
      // ========================
      case 'login':
        if (environment.MOCK_API)
          this.URLData = { URL: 'local.json', method: 'GET' }
        else
          this.URLData = { URL: '/auth/login', method: 'GET' }
        break;
      case 'authenticate':
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'excursionList.json', method: 'GET' }
        else
          this.URLData = { URL: 'api/authenticate', method: 'POST' }
        break;
      case 'getExcursion':
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'excursionList.json', method: 'GET' }
        else
          this.URLData = { URL: "api/excrusions", method: 'GET' };
        break;
      case 'saveExcursion':
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'excursionList.json', method: 'GET' }
        else
          this.URLData = { URL: "api/excrusions", method: 'POST' };
        break;
      case 'editExcursion':
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'excursionList.json', method: 'GET' }
        else
          this.URLData = { URL: "api/excrusions", method: 'PUT' };
        break;
      case 'deleteExcursion':
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'excursionList.json', method: 'GET' }
        else
          this.URLData = { URL: "api/excrusions/", method: 'DELETE' };
        break;
      case 'getDining':
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'RestaurantList.json', method: 'GET' }
        else
          this.URLData = { URL: "", method: 'GET' };
        break;
      case 'getMealList':
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'MealList.json', method: 'GET' }
        else
          this.URLData = { URL: "", method: 'GET' };
        break;
      case "searchExcursion":
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'excursionList.json', method: 'GET' }
        else
          this.URLData = { URL: "api/excrusions/criteria", method: 'GET' };
        break;
      case "getBookingList":
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'bookingList.json', method: 'GET' }
        else
          this.URLData = { URL: "api/bookings", method: 'GET' };
        break;
      case "getTravellerList":
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'travellerList.json', method: 'GET' }
        else
          this.URLData = { URL: "api/travellers", method: 'GET' };
        break;
      case "saveBooking":
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'travellerList.json', method: 'GET' }
        else
          this.URLData = { URL: "api/bookings", method: 'POST' };
        break;
      case "cancelBooking":
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'travellerList.json', method: 'DELETE' }
        else
          this.URLData = { URL: "api/bookings/cancellation", method: 'PUT' };
        break;

      case "getExcusionDetailById":
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'bookingList.json', method: 'GET' }
        else
          this.URLData = { URL: "api/excrusions/", method: 'GET' };
        break;

      case "updateBookedExcursion":
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'bookingList.json', method: 'GET' }
        else
          this.URLData = { URL: "api/bookings", method: 'PUT' };
        break;

      case 'updateTravellerList':
        if (environment.MOCK_API)
          this.URLData = { URL: this.MOCK_BASE_URL + 'excursionList.json', method: 'GET' }
        else
          this.URLData = { URL: "api/travellers", method: 'PUT' };
        break;
    }

    return this.URLData;
  }
}
