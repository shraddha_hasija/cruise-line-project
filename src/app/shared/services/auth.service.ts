import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  userDetails: any = null;
  constructor(private router: Router) { }

  isUserLoggedIn() {
    if (sessionStorage.getItem('currentUser') || localStorage.getItem('currentUser')) {
      return true;
    }
    return false;
  }

  loginUser(userData, rememberFlag) {
    sessionStorage.removeItem('currentUser');
    localStorage.removeItem('currentUser');
    if (rememberFlag) {
      localStorage.setItem('currentUser', JSON.stringify(userData));
    } else {
      sessionStorage.setItem('currentUser', JSON.stringify(userData));
    }
    this.userDetails = userData;
    //redirect to first page after login
    this.router.navigateByUrl('');
  }

  getUserData() {
    let userData;
    if (sessionStorage.getItem('currentUser')) {
      userData = sessionStorage.getItem('currentUser');
    } else if (localStorage.getItem('currentUser')) {
      userData = localStorage.getItem('currentUser');
    } else {
      return false;
    }

    this.userDetails = JSON.parse(userData);
    return this.userDetails;
  }

  logoutUser() {
    sessionStorage.removeItem('currentUser');
    localStorage.removeItem('currentUser');
    this.userDetails = null;
    // redirect to login page after logout
    this.router.navigateByUrl('');
  }

}
