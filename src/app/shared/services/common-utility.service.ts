import { Injectable } from "@angular/core";
import { isNumber } from "util";



@Injectable()
export class CommonUtilityService {


    filterByKey(searchValue: any, data: any, searchKey: string): any {
        let listData = data;
        let term = searchValue && searchValue.length ? searchValue.toLowerCase() : "";
        listData = listData.filter(obj => {
            let val = obj[searchKey] ? obj[searchKey].toLowerCase() || obj[searchKey] : "";
            return val.indexOf(term) > -1;
        });
        console.log(listData);
        return listData;
    }

    filterById(searchValue: any, data: any, searchKey: string) {
        let listData = data;
        let term = searchValue && isNumber(parseInt(searchValue)) ? parseInt(searchValue) : null;
        listData = listData.filter(obj => {
            let val = obj[searchKey] ? parseInt(obj[searchKey]) : "";
            return val === term;
        });
        console.log(listData);
        return listData;
    }

    filterByDates(startDate: any, endDate: any, data: any): any {
        let listData = data;
       
        listData = listData.filter(obj => {
            // obj[searchKey]
            // let val = obj[searchKey] ? obj[searchKey].toLowerCase() : "";
            // return val.indexOf(term) > -1;
            let dateObj = obj["activeDuration"];

            if (!dateObj["startDate"] || !+dateObj["startDate"].trim().length
                || !dateObj["endDate"] || !+dateObj["endDate"].trim().length) {
            } else {
                return +startDate > +new Date(dateObj["startDate"]) && +new Date(dateObj["endDate"]) > +endDate;
            }
        });
        return listData;
       
    }

    sortDataAsc(key, data) {
        let dataArr = JSON.parse(JSON.stringify(data));
        if (dataArr.length) {
            let result = dataArr.sort((obj1, obj2) => {
                if (obj1[key] < obj2[key]) {
                    return -1; //desc
                } else if (obj1[key] > obj2[key]) {
                    return 1; // asc
                } else {
                    return 0;
                }
            });
            return result;
        }
        return data;
    }

    sortDataDesc(key, data) {
        let dataArr = JSON.parse(JSON.stringify(data));
        if (dataArr.length) {
            let result = dataArr.sort((obj1, obj2) => {
                if (obj1[key] < obj2[key]) {
                    return 1;
                } else if (obj1[key] > obj2[key]) {
                    return -1;
                } else {
                    return 0;
                }
            });
            return result
        }
        return data;
    }
}