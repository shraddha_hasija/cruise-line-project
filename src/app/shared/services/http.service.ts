/* This Service class used as wrapper for differnet API call which indirectly calls of http post,delete,get etc. calls.*/
import { Injectable } from '@angular/core';
import { Response, Headers, RequestOptions } from '@angular/http';
import { HttpHeaders, HttpClient } from '@angular/common/http';
// import { Router } from "@angular/router";

import { map, catchError, mergeMap } from 'rxjs/operators';
import { URLService } from "./url.service";
import { environment } from "../../../environments/environment";
import { Observable } from 'rxjs';
// import { AuthService } from './auth.service';


@Injectable()
export class HTTPService {
  /*GET_LOCAL_DATA - this flag = true used if you want to run application in local enviorment.Local data(JSON files from data) is used in the application in this case.
  if this flag = false actual web apis are used and application will be run in application server. */
  public GET_LOCAL_DATA: boolean = true;

  constructor(private http: HttpClient, private urlService: URLService) {

  }

  getAuthToken() {
    return sessionStorage.getItem("authToken") || "Bearer ";
  }

  makeGetCall(endpoint, queryURL) {
    let URL = '';
    if (!environment.MOCK_API) {
      if (queryURL) {
        URL = endpoint + queryURL;
      } else {
        URL = endpoint;
      }
    } else {
      URL = endpoint;
    }

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'Authorization': this.getAuthToken(),
        'Pragma': 'no-cache'
      })
    };

    return this.http.get(URL, httpOptions)
      .pipe(
        map((response: Response) => {
          this.hideLoader();
          return response;
        }),
        catchError(this.handleError.bind(this))
      )
  }

  makePostCall(endpoint: any, data: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.getAuthToken(),
      })
    };

    return this.http.post(endpoint, data, httpOptions)
      .pipe(
        map((response: Response) => {
          this.hideLoader();
          return response;
        }),
        catchError(this.handleError.bind(this))
      )
  }

  makePutCall(endpoint: any, data: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.getAuthToken(),
      })
    };

    return this.http.put(endpoint, data, httpOptions)
      .pipe(
        map((response: Response) => {
          this.hideLoader();
          return response;
        }),
        catchError(this.handleError.bind(this))
      )
  }

  makeDeleteCall(endpoint, queryURL) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.getAuthToken(),
      })
    };

    let URL = '';
    if (!environment.MOCK_API) {
      if (queryURL) {
        URL = endpoint + queryURL;
      } else {
        URL = endpoint;
      }
    } else {
      URL = endpoint;
    }

    return this.http.delete(URL, httpOptions)
      .pipe(
        map((response: Response) => {
          this.hideLoader();
          return response;
        }),
        catchError(this.handleError.bind(this))
      )
  }

  call(serviceName: string, payload?: any, queryParam?: any) {
    let URLData = this.urlService.getAPIData(serviceName);
    let queryVal = queryParam ? "/" + queryParam : "";
    this.showLoader();

    if (URLData.method == 'GET') {
      if (environment.MOCK_API) {
        return this.makeGetCall(URLData.URL, payload);
      }
      if (this.urlService.BASE_URL === null) {
        return this.http.get('assets/config.json').pipe(mergeMap((response: any) => {
          let BASE_URL = response.BASE_URL;
          this.urlService.BASE_URL = BASE_URL;
          this.showLoader();
          return this.makeGetCall(BASE_URL + URLData.URL, payload);
        }));
      } else {
        return this.makeGetCall(this.urlService.BASE_URL + URLData.URL, payload);
      }
    }
    if (URLData.method == 'POST') {
      if (this.urlService.BASE_URL === null) {
        return this.http.get('assets/config.json').pipe(mergeMap((response: any) => {
          let BASE_URL = response.BASE_URL;
          this.urlService.BASE_URL = BASE_URL;
          this.showLoader();
          return this.makePostCall(BASE_URL + URLData.URL, payload);
        }));
      } else {
        return this.makePostCall(this.urlService.BASE_URL + URLData.URL, payload);
      }
    }
    if (URLData.method == 'PUT') {
      if (this.urlService.BASE_URL === null) {
        return this.http.get('assets/config.json').pipe(mergeMap((response: any) => {
          let BASE_URL = response.BASE_URL;
          this.urlService.BASE_URL = BASE_URL;
          this.showLoader();
          return this.makePutCall(BASE_URL + URLData.URL + queryVal, payload);
        }));
      } else {
        return this.makePutCall(this.urlService.BASE_URL + URLData.URL + queryVal, payload);
      }
    }

    if (URLData.method == 'DELETE') {
      if (this.urlService.BASE_URL === null) {
        return this.http.get('assets/config.json').pipe(mergeMap((response: any) => {
          let BASE_URL = response.BASE_URL;
          this.urlService.BASE_URL = BASE_URL;
          this.showLoader();
          return this.makeDeleteCall(BASE_URL + URLData.URL, payload);
        }));
      } else {
        return this.makeDeleteCall(this.urlService.BASE_URL + URLData.URL, payload);
      }
    }

  }
  showLoader() {
    //var x = document.getElementById("loader");
    //x.style.display = "block";
  }

  hideLoader() {
    //var x = document.getElementById("loader");
    //x.style.display = "none";
  }

  handleError(error: any): any {
    // this.router.navigate(['']);
    this.hideLoader();
    console.log(error);

    if (error && error.status == 401) {
      this.getAuthToken();
    }
    throw (error.message || error);
  }

  generateAuthToken() {
    let req = {
      "username": "user",
      "password": "user"
    };
    this.call("authenticate", req).subscribe((data: any) => {
      console.log(data);
      sessionStorage.setItem("authToken", "Bearer " + data.id_token);
    });
  }
}
