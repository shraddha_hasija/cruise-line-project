import { Injectable } from "@angular/core";
import { Subject, Observable } from "rxjs";



@Injectable()
export class SharedDataService {

    private subject = new Subject<any>();
    private excursionUserData: any = [];
    private bookingDetails: any = {};

    constructor() { }

    getBookingDetails() {
        return this.bookingDetails;
    }

    setBookingDetails(data) {
        this.bookingDetails = data;
    }
    
    getExcursionUserData() {
        return this.excursionUserData;
    }

    setExcursionUserData(data) {
        this.excursionUserData = data;
    }

    notify(data) {
        this.subject.next(data);
    }

    subscribeNotifications(): Observable<any> {
        return this.subject;
    }
}