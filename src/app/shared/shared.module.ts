import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTPService } from "./services/http.service";
import { URLService } from "./services/url.service";
import { HttpClientModule } from '@angular/common/http';
import { CommonUtilityService } from './services/common-utility.service';
import { AlertComponent } from './utilities/alert.component';
import { AlertModule } from 'ngx-bootstrap';
import { SharedDataService } from './services/shared-data.service';
import { AuthGuard } from './guards/auth.guard';
import { BookExcursionAuthGuard } from './guards/book-excursion-auth.guard';

@NgModule({
  imports: [
    CommonModule,
    AlertModule,
    HttpClientModule
  ],
  declarations: [AlertComponent],
  providers: [AuthGuard, HTTPService, URLService, CommonUtilityService,
    SharedDataService, BookExcursionAuthGuard],
  exports: [AlertComponent],
  entryComponents: []
})
export class SharedModule { }
