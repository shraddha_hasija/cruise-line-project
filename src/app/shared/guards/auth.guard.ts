/* This Service class used for auth guard for login.Redirecting user if not logged in*/
import { debug } from 'util';
import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AuthService } from '../services/auth.service';


@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private authservice: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let url: string = state.url;
        return this.checkLogin(url);
    }
    checkLogin(url: string): boolean {
        let allowedPaths = ['/login'];
        if (this.authservice.isUserLoggedIn()) {
            if (allowedPaths.indexOf(url) < 0) {
                // logged in so return true
            } else {
                //redirect to next page after login
                //this.router.navigateByUrl('');
            }
        } else if (allowedPaths.indexOf(url) < 0) {
            // not logged in so redirect to login page with the return url
            this.router.navigateByUrl('');
        }
        return true;
    }

}
