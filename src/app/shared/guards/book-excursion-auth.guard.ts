/* This Service class used for auth guard for login.Redirecting user if not logged in*/
import { debug } from 'util';
import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { SharedDataService } from '../services/shared-data.service';


@Injectable()
export class BookExcursionAuthGuard implements CanActivate {

    constructor(private router: Router, private dataService: SharedDataService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let data = this.dataService.getExcursionUserData();
        let isValid = data && data.length ? true : false;
        return isValid;
    }
}
