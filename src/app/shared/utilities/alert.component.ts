import { Component, Input } from "@angular/core";


@Component({
    selector: 'app-alert',
    template: `<div class="parentDiv text-center row m-t-20">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                    <alert type="{{alertType}}">
                        <strong>{{titleText}}</strong> 
                        <div>{{message}}</div>
                    </alert>
                    </div>
                    <div class="col-md-2"></div>
                </div>`,
    styles: ['.parentDiv {position: fixed;right: -50px;top: 0;width: 500px;z-index: 1000}']
})
export class AlertComponent {
    @Input() titleText: string = "";
    @Input() message: string = "";
    @Input() alertType: string = "info";

    constructor() {

    }
}