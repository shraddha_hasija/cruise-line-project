import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from 'src/app/shared/services/common-utility.service';
import { HTTPService } from 'src/app/shared/services/http.service';
import { URLService } from 'src/app/shared/services/url.service';

@Component({
  selector: 'app-list-of-meal',
  templateUrl: './list-of-meal.component.html',
  styleUrls: ['./list-of-meal.component.css']
})
export class ListOfMealComponent implements OnInit {

  mealList: any = [];
  selectedMealList: any = [];
  actionsArr = ["Deactivate", "Activate"];
  noOfItems = 10;
  currPage = 1;

  constructor(private commonService: CommonUtilityService, private httpService: HTTPService, private urlService: URLService) { }

  ngOnInit() {
    this.getMealList();
  }
  sortData(order, key) {
    let dataArr = JSON.parse(JSON.stringify(this.mealList));
    if (order === "asc") {
      this.mealList = this.commonService.sortDataAsc(key, dataArr);
    }
    else if (order === "desc") {
      this.mealList = this.commonService.sortDataDesc(key, dataArr);
    }
  }
  refresh() {
    this.getMealList();
  }

  getMealList() {
    this.httpService.call(this.urlService.getAPIData('getMealList')).subscribe((data) => {
      this.mealList = data;
    });
  }

  setCheckboxSelection(event, id) {
    if (event.target.checked) {
      this.selectedMealList.push(id);
    } else {
      this.selectedMealList.splice(this.selectedMealList.indexOf(id));
    }
    console.log(this.selectedMealList);
  }

}
