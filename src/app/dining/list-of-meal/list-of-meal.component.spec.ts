import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfMealComponent } from './list-of-meal.component';

describe('ListOfMealComponent', () => {
  let component: ListOfMealComponent;
  let fixture: ComponentFixture<ListOfMealComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfMealComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfMealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
