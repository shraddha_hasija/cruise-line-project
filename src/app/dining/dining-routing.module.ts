import { RouterModule, Routes } from "@angular/router";
import { DiningComponent } from "./dining.component";
import { AddnewDiningComponent } from "./add-new-dining/add-new-dining.component";

const routerConfig: Routes = [
    {
        path: 'list',
        component: DiningComponent
    },
    {
        path: '',
        redirectTo: 'list'
    },
    {
        path: 'add',
        component: AddnewDiningComponent
    }
];

export const diningRoutes = RouterModule.forChild(routerConfig);