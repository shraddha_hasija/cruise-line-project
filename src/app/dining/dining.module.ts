import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { DiningComponent } from "./dining.component";
import { diningRoutes } from "./dining-routing.module";
import { NgxPaginationModule } from "ngx-pagination";
import { AddnewDiningComponent } from './add-new-dining/add-new-dining.component';
import { NgxEditorModule } from "ngx-editor";
import { AddMealComponent } from './add-meal/add-meal.component';
import { TabsModule,BsDatepickerModule,BsDropdownModule, TimepickerModule } from "ngx-bootstrap";
import { ListOfDiningComponent } from './list-of-dining/list-of-dining.component';
import { ListOfMealComponent } from './list-of-meal/list-of-meal.component';
import { SharedModule } from "../shared/shared.module";

@NgModule({
    declarations: [DiningComponent, AddnewDiningComponent, AddMealComponent, ListOfDiningComponent, ListOfMealComponent],
    imports: [
        FormsModule,
        CommonModule,
        diningRoutes,
        NgxPaginationModule,
        NgxEditorModule,
        TimepickerModule,
        TabsModule,
        BsDropdownModule.forRoot(),
        BsDatepickerModule,
        SharedModule
        // TimepickerModule
    ],
    exports: [],
    entryComponents: []
})
export class DiningModule {

}