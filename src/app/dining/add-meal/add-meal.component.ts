import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-meal',
  templateUrl: './add-meal.component.html',
  styleUrls: ['./add-meal.component.css']
})
export class AddMealComponent implements OnInit {

  formSubmitted: boolean = false;

  mealTitle = "";
  reservationTypeList = ['Required', 'Not Required'];
  cuisineTypeList = ["American", "Indian", "Greek", "British", "Chinese", "French", "Italian", "Japanese", "Maxican", "Spanish", "Thai", "Turkish", "Moroccan"]
  selectedReservationType = "";
  selectedCuisionType = "";

  // priceTypeList = ['Fixed', 'A-La carte'];
  // selectedPriceType = "";
  // cancellationFee = "";
  // pricePerPerson = "";
  preBookingCapacity: any = "";
  totalCapacity = "";
  startDate = moment().format('MMM DD YYYY');
  endDate = moment().format('MMM DD YYYY');

  isIntranet: boolean = false;
  isCustomerWebsite: boolean = false;
  isPortBooking: boolean = false;
  isReservationSoftware: boolean = false;

  // step 2
  description = "";
  editorConfig = {
    minHeight: 100,
    width: 300,
    placeholder: "Enter text here..",
    spellcheck: true
  }
  imagePathList = [];

  // @Output() mealDetailsEmitter = new EventEmitter();
  ///

  selectedCuisineList: any = [];
  mealStartTime: any = "";
  mealEndTime: any = "";
  mealAvgTime: any = '';
  pricePerPerson: any = '';
  cancellationFee: any = '';
  dpConfig = {
    dateInputFormat: 'MMM DD YYYY',
    containerClass: 'theme-red'
  }
  // languageList = ['English', 'German', 'Spanish', 'French'];
  // defaultLanguage = "English";
  @Output() showDiningScreenEmitter = new EventEmitter();
  @Output() addedMealListEmitter = new EventEmitter();
  @Input() addedMealList: any = [];
  mediaFileList = [{ url: "" }];
  menuDetails = '';
  status: any = "ACTIVE";

  mealId = null;
  restId = null;
  // restaurantId in meal

  addMealObj = {
    mealName: "",
    reservationRequired: false,
    fromTime: 0,
    toTime: 0,
    avgDuration: 0,
    pricePerPerson: 0,
    cancellationFees: 0,
    noOfGuests: 0,
    startDate: "",
    endDate: "",
    fromInternet: false,
    fromCustomerWebsite: false,
    fromPortBooking: false,
    fromReservationSoftware: false,
    bookingInstructions: "",
    cuisines: [],
    media: [],
    isActive: true,
    menuText: this.menuDetails,
    id: this.mealId,
    restaurantId: this.restId
  }

  saveMealMsg = "Save Meal";
  isUpdateingAnyMeal = false;
  positionOfMealToUpdate = 0;


  constructor(private datePipe: DatePipe) { }

  ngOnInit() {
    this.selectedReservationType = this.reservationTypeList[0];
    // this.selectedPriceType = this.priceTypeList[0];
  }


  updateMealDetails(mealDetails, index) {
    this.positionOfMealToUpdate = index;
    this.isUpdateingAnyMeal = true;
    this.saveMealMsg = "Update Meal";
    let reservationRequire = "Required";
    if (mealDetails.reservationRequired === false) {
      reservationRequire = "Not Required";
    }

    this.status = "ACTIVE";
    if (mealDetails.isActive === false) {
      this.status = "INACTIVE"
    }

    let cuisineData = [];
    if (mealDetails.cuisines) {
      for (let index = 0; index < mealDetails.cuisines.length; index++) {
        cuisineData.push(mealDetails.cuisines[index].cuisineName);
      }
    }

    // this.startDate = this.datePipe.transform(this.startDate, "MMM dd yyyy");
    // this.endDate = this.datePipe.transform(this.endDate, "MMM dd yyyy"); //output : Dec-12-2018

    this.mealTitle = mealDetails.mealName;
    this.selectedReservationType = reservationRequire;
    this.mealStartTime = mealDetails.fromTime;
    this.mealEndTime = mealDetails.toTime;
    this.mealAvgTime = mealDetails.avgDuration;
    this.pricePerPerson = mealDetails.pricePerPerson;
    this.cancellationFee = mealDetails.cancellationFees;
    this.preBookingCapacity = mealDetails.noOfGuests;
    this.startDate = mealDetails.startDate;
    this.endDate = mealDetails.endDate;

    this.isIntranet = mealDetails.fromInternet;
    this.isCustomerWebsite = mealDetails.fromCustomerWebsite;
    this.isPortBooking = mealDetails.fromPortBooking;
    this.isReservationSoftware = mealDetails.fromReservationSoftware;
    this.description = mealDetails.bookingInstructions;
    // this.status = mealDetails.isActive;
    this.menuDetails = mealDetails.menuText;
    this.mediaFileList = mealDetails.media;
    this.selectedCuisineList = cuisineData;
    this.mealId = mealDetails.id;
    this.restId = mealDetails.restaurantId;
  }

  deleteAddedMealFromList(position) {
    this.addedMealList.splice(position, 1);
  }

  goBackTODiningScreen() {
    this.addedMealListEmitter.emit(this.addedMealList);
    this.showDiningScreenEmitter.emit(true);
  }

  onCuisineTypeChange(cuision) {
    this.selectedCuisionType = cuision;
  }

  addCuisineInList() {
    if (this.selectedCuisionType && this.selectedCuisionType !== '' && !this.selectedCuisineList.some(x => x === this.selectedCuisionType)) {
      this.selectedCuisineList.push(this.selectedCuisionType);
    }
  }

  deleteCruiseItem(position) {
    this.selectedCuisineList.splice(position, 1);
  }

  updateTimeFormat(value) {
    if (value == "mealStartTime") {
      this.mealStartTime = moment(this.mealStartTime).format("HH:mm");
    } else {
      this.mealEndTime = moment(this.mealEndTime).format("HH:mm");
    }
    console.log(this.mealStartTime);
  }

  onAddMealToList() {
    // if (this.validateForm()) {

    let reservationRequire = false;
    if (this.selectedReservationType === "Required") {
      reservationRequire = true;
    }
    let cuisineList = [];
    for (let index = 0; index < this.selectedCuisineList.length; index++) {
      let newType = {
        cuisineName: this.selectedCuisineList[index],
      };
      cuisineList.push(newType);
    }
    let activeStatus = true;
    if (this.status === "INACTIVE") {
      activeStatus = false;
    }
    // this.startDate = this.datePipe.transform(this.startDate, "yyyy-MM-dd");
    // this.endDate = this.datePipe.transform(this.endDate, "yyyy-MM-dd");
    //
    // status has to set here
    //

    this.addMealObj = {
      mealName: this.mealTitle,
      reservationRequired: reservationRequire,
      fromTime: this.mealStartTime,
      toTime: this.mealEndTime,
      avgDuration: this.mealAvgTime,
      pricePerPerson: this.pricePerPerson,
      cancellationFees: this.cancellationFee,
      noOfGuests: this.preBookingCapacity,
      startDate: this.startDate,
      endDate: this.endDate,

      fromInternet: this.isIntranet,
      fromCustomerWebsite: this.isCustomerWebsite,
      fromPortBooking: this.isPortBooking,
      fromReservationSoftware: this.isReservationSoftware,
      bookingInstructions: this.description,
      cuisines: cuisineList,
      media: this.mediaFileList,
      isActive: activeStatus,
      menuText: this.menuDetails,
      id: this.mealId,
      restaurantId: this.restId
    };

    if (this.isUpdateingAnyMeal) {
      this.addedMealList[this.positionOfMealToUpdate] = this.addMealObj;
      this.saveMealMsg = "Save Meal";
      this.isUpdateingAnyMeal = false;
      this.positionOfMealToUpdate = 0;
    } else {
      this.addedMealList.push(this.addMealObj);
    }

    // console.log(this.datePipe.transform(this.endDate,"yyyy-MM-dd")); //output : 2018-02-13
    // console.log(this.datePipe.transform(this.startDate,"yyyy-MM-dd")); //output : 2018-02-13

    // console.log(this.datePipe.transform(this.endDate,"MMM dd yyyy")); //output : 2018-02-13
    // console.log(this.datePipe.transform(this.startDate,"MMM dd yyyy")); //output : 2018-02-13

    this.onClearMealData();
    // } else {
    //   console.log("meal validation");
    // }
  }


  validateForm() {
    let isFormValid = this.validateMealTitle() &&
      this.validateMealStartTime() &&
      this.validateMealEndTime() &&
      this.validateStartDate() &&
      this.validateEndDate() &&
      //this.validateTransType() &&
      this.validatePrice() &&
      this.validateCancellationFee() &&
      this.preBookingTotalCapacity();
    return isFormValid;
  }
  validateMealStartTime() {
    if (!this.mealStartTime || !this.mealStartTime.length) {
      return false;
    }
    return true;
  }

  validateMealEndTime() {
    if (!this.mealEndTime || !this.mealEndTime.length) {
      return false;
    }
    return true;
  }

  onClearMealData() {
    this.mealTitle = '';
    this.selectedReservationType = this.reservationTypeList[0];
    this.mealStartTime = '';
    this.mealEndTime = '';
    this.mealAvgTime = '';
    this.pricePerPerson = '';
    this.cancellationFee = '';
    this.preBookingCapacity = '';
    this.startDate = '';
    this.endDate = '';

    this.isIntranet = false;
    this.isCustomerWebsite = false;
    this.isPortBooking = false;
    this.isReservationSoftware = false;
    this.description = '';
    this.selectedCuisineList = [];
    this.selectedCuisionType = '';
    this.status = "ACTIVE";
    this.menuDetails = '';
    this.mediaFileList = [];
    this.mealId = null;
    this.restId = null;
    //
    this.addMealObj = {
      mealName: "",
      reservationRequired: false,
      fromTime: 0,
      toTime: 0,
      avgDuration: 0,
      pricePerPerson: 0,
      cancellationFees: 0,
      noOfGuests: 0,
      startDate: "",
      endDate: "",

      fromInternet: false,
      fromCustomerWebsite: false,
      fromPortBooking: false,
      fromReservationSoftware: false,
      bookingInstructions: "",
      cuisines: [],
      media: [],
      isActive: true,
      menuText: this.menuDetails,
      id: this.mealId,
      restaurantId: this.restId
    }
  }

  validateMealTitle() {
    if (!this.mealTitle || !this.mealTitle.length) {
      return false;
    } else {
      return true;
    }
  }
  counter = 1;
  addMediaFile() {
    this.counter = this.counter + 1;

    let newMediaFile = {
      url: "",
      index: this.counter
    };
    this.mediaFileList.push(newMediaFile);
    console.log(this.mediaFileList);
  }

  removeMediaFile(i) {
    if (this.mediaFileList.length > 1) {
      this.mediaFileList.splice(i, 1);
    }
  }
  // onPriceTypeChange(type: any) {
  //   this.selectedPriceType = type;
  // }
  onReservationTypeChange(type: any) {
    this.selectedReservationType = type;
  }

  validatePrice() {
    if (!this.pricePerPerson) {
      return false;
    } else {
      return true;
    }
  }
  validateCancellationFee() {
    if (!this.cancellationFee) {
      return false;
    } else {
      return true;
    }
  }

  validatePreBooking() {
    if (!this.preBookingCapacity) {
      return false;
    } else {
      return true;
    }
  }


  preBookingTotalCapacity() {
    if (!this.preBookingCapacity) {
      return false;
    } else {
      return true;
    }
  }
  validateStartDate() {
    if (!this.startDate) {
      return false;
    } else {
      return true;
    }
  }
  validateEndDate() {
    if (!this.endDate) {
      return false;
    } else {
      return true;
    }
  }


  // onAddMeal() {
  //   this.formSubmitted = true;
  //   if (this.validateForm()) {
  //     let mealData = {};

  //     mealData = {
  //       "mealTitle": this.mealTitle,
  //       "selectedReservationType": this.selectedReservationType,
  //       // "selectedPriceType": this.selectedPriceType,
  //       "pricePerPerson": this.pricePerPerson,
  //       "cancellationFee": +this.cancellationFee,
  //       "preBookingCapacity": this.preBookingCapacity,
  //       "totalCapacity": +this.totalCapacity,
  //       "startDate": this.startDate,
  //       "endDate": this.endDate,

  //       "interfaces": {
  //         web: this.isCustomerWebsite,
  //         app: this.isIntranet,
  //         chat: this.isPortBooking,
  //         kisok: this.isReservationSoftware
  //       },
  //       "status": this.status,
  //       "content": {
  //         "description": this.description,
  //       },
  //       "mediaMeal": this.imagePathList,
  //     }
  //     this.mealDetailsEmitter.emit(mealData);
  //     // console.log(mealData);

  //   } else {
  //     console.log("Fom is not validate");
  //   }
  // }
}
