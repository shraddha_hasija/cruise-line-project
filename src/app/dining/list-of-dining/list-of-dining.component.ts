import { Component, OnInit, Output, EventEmitter, SimpleChanges, Input } from '@angular/core';
import { CommonUtilityService } from 'src/app/shared/services/common-utility.service';
import { HTTPService } from 'src/app/shared/services/http.service';
import { URLService } from 'src/app/shared/services/url.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-list-of-dining',
  templateUrl: './list-of-dining.component.html',
  styleUrls: ['./list-of-dining.component.css']
})
export class ListOfDiningComponent implements OnInit {

  diningList: any = [];
  actionsArr = ["Activate", "Inactivate"];
  selectedAction: string = "Inactivate";
  selectedExcursion = [];
  noOfItems = 10;
  currPage = 1;
  deleteConfrimDialog: BsModalRef;
  selectedDiningForDelete = 0;
  alert = {
    message: '',
    type: "",
    title: ""
  };
  isShowAlert = false;

  @Output() selectedDiningDataEmitter: EventEmitter<any> = new EventEmitter();
  @Input() reloadList: boolean = false;

  constructor(private commonService: CommonUtilityService, private httpService: HTTPService,
    private bsModalService: BsModalService, private urlService: URLService) { }

  ngOnInit() {
    this.getDining();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.reloadList && changes.reloadList.currentValue && !changes.reloadList.isFirstChange()) {
      this.refresh();
    }
  }

  refresh() {
    this.getDining();
  }

  getCapacity(tableList) {
    let totalCapacity = 0;
    tableList.forEach(element => {
      totalCapacity = totalCapacity + element.maxSeats;
    });
    return totalCapacity;
  }

  gotoEditDinig(diningData) {
    this.selectedDiningDataEmitter.emit(diningData);
  }

  showConfirmDeleteDialog(no, template) {
    this.selectedDiningForDelete = no;
    this.deleteConfrimDialog = this.bsModalService.show(template, { class: 'modify-booking-modal modal-md' });
  }

  deleteSelectedDining() {
    let url = this.urlService.getAPIData('deleteDining');
    console.log(url);
    this.httpService.call(url, this.selectedDiningForDelete).subscribe((data) => {
      this.alert = {
        type: "success",
        title: "Success",
        message: "Dining deleted successfully"
      }
      this.refresh();
      this.showNotification();
      this.deleteConfrimDialog.hide();
    }, (error) => {
      this.alert = {
        type: "error",
        title: "Failed",
        message: "Some error occured"
      };
      this.showNotification();
      this.deleteConfrimDialog.hide();
    });
  }

  getDining() {
    // this.httpService.call(this.urlService.getAPIData('getDining')).subscribe((data) => {
    //   this.diningList = data;
    // });
    this.httpService.call(this.urlService.getAPIData('getRestaurantList')).subscribe((data) => {
      // this.diningList = data;
      console.log("data is ==>" + data);
      this.diningList = data;

    });
  }

  setCheckboxSelection(event, id) {
    if (event.target.checked) {
      this.selectedExcursion.push(id);
    } else {
      this.selectedExcursion.splice(this.selectedExcursion.indexOf(id));
    }
    console.log(this.selectedExcursion);
  }
  showNotification() {
    this.isShowAlert = true;
    setTimeout(() => {
      this.isShowAlert = false;
    }, 3000);

  }
}

// applySelection() {
//   console.log(this.selectedAction);
//   console.log(this.selectedExcursion);
// }

// onSelectionChange(value) {
//   console.log(value);
//   this.selectedAction = value;
// }

// sortData(order, key) {
//   let dataArr = JSON.parse(JSON.stringify(this.diningList));
//   if (order === "asc") {
//     this.diningList = this.commonService.sortDataAsc(key, dataArr);
//   }
//   else if (order === "desc") {
//     this.diningList = this.commonService.sortDataDesc(key, dataArr);
//   }
// }