import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfDiningComponent } from './list-of-dining.component';

describe('ListOfDiningComponent', () => {
  let component: ListOfDiningComponent;
  let fixture: ComponentFixture<ListOfDiningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfDiningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfDiningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
