import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';
import { NgForm } from '@angular/forms';
import * as moment from 'moment';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { CommonUtilityService } from 'src/app/shared/services/common-utility.service';
import { HTTPService } from 'src/app/shared/services/http.service';
import { URLService } from 'src/app/shared/services/url.service';
import { moduleDef } from '@angular/core/src/view';
import { Pipe, PipeTransform } from '@angular/core';
import { UpperCasePipe, DatePipe } from '@angular/common';

@Component({
  selector: 'app-add-new-dining',
  templateUrl: './add-new-dining.component.html',
  styleUrls: ['./add-new-dining.component.css']
})
export class AddnewDiningComponent implements OnInit {

  formSubmitted: boolean = false;

  editorConfig = {
    minHeight: 100,
    width: 300,
    placeholder: "Enter text here..",
    spellcheck: true
  }
  diningList: any = [];
  actionsArr = ["Deactivate", "Activate"];
  noOfItems = 10;
  currPage = 1;

  restaurantName = "";
  openingTime = "";
  closingTime = "";
  description = "";

  totalCapacity = "";
  price = "";
  cancellationFee = "";


  isValid = {
    isRestName: true,
    isOpenTime: true,
    isCloseTime: true,
    isCapacity: true,
    isPrice: true,
    isCancellationFee: true
  };
  isDataValidate = true;
  mediaFileList = [{ url: "" }];
  modalRef: BsModalRef;
  // shapeList = ["Oval", "Rectangle", "Circular", "Square"];
  // OrientationList = ["Horizontal", "vertical"];

  shapeList = ["OVAL", "RECTANGLE", "CIRCULAR", "SQUARE"];
  OrientationList = ["HORIZONTAL", "VERTICAL"];

  selectedShape = "";
  selectedOrientation = "";
  minSeat: any = 0;
  maxSeat: any = 0;
  isMovable = false;
  isReservationRequire = false;
  isSmokingAllow = false;

  tableId = null;
  restId = null;
  isEdit = false;


  addTableObj = {
    tableShape: "",
    orientation: "",
    minSeats: "",
    maxSeats: "",
    movable: false,
    reservationRequired: false,
    smoking: false,
    id: this.tableId,
    restaurantId: this.restId
  }

  needToShowAddMealComponent = false;
  addedMealDetailList: any = [];
  addedTableList: any = [];
  alert = {
    message: '',
    type: "",
    title: ""
  };
  isShowAlert = false;
  languageList = ['English', 'German', 'Spanish', 'French'];
  saveTableMsg = "Save Meal";
  isUpdateingAnyTable = false;
  selectedLanguage = 'English';
  positionOfTableUpdadte = 0;
  @Input() updateDiningDetails = {};
  @Output() resetTab: EventEmitter<any> = new EventEmitter();
  @Output() notify: EventEmitter<any> = new EventEmitter();

  // @Output() mealListEmitter = new EventEmitter();

  constructor(private commonService: CommonUtilityService, private httpService: HTTPService,
    private urlService: URLService, private modalService: BsModalService, private datePipe: DatePipe) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.updateDiningDetails && !changes.updateDiningDetails.isFirstChange()) {
      console.log(changes);
      this.isEdit = true;
      this.setDiningData(changes.updateDiningDetails.currentValue);
      this.restaurantName = changes.updateDiningDetails.currentValue.restaurantName;
      console.log("==>" + this.restaurantName);
      this.restId = changes.updateDiningDetails.currentValue.id;

    }
  }
  setDiningData(diningDetails: any): any {

    this.restaurantName = diningDetails.restaurantName;
    this.openingTime = diningDetails.openingTime;
    this.closingTime = diningDetails.closingTime;
    this.description = diningDetails.restaurantDescription;
    this.addedTableList = diningDetails.diningTables;
    this.mediaFileList = diningDetails.media;

    if (diningDetails.diningMeals && diningDetails.diningMeals.length > 0) {
      for (let index = 0; index < diningDetails.diningMeals.length; index++) {
        diningDetails.diningMeals[index].endDate = this.datePipe.transform(diningDetails.diningMeals[index].endDate, "MMM dd yyyy");
        diningDetails.diningMeals[index].startDate = this.datePipe.transform(diningDetails.diningMeals[index].startDate, "MMM dd yyyy"); //output : Dec-12-2018
      }
    }
    this.addedMealDetailList = diningDetails.diningMeals;
  }

  onShapeChange(shape) {
    this.selectedShape = shape;
  }

  onLangChange(language) {
    this.selectedLanguage = language;
  }
  onOrientationChange(orientation) {
    this.selectedOrientation = orientation;
  }

  updateTableData(tableDetails, position, template) {
    this.saveTableMsg = "Update Table";
    this.isUpdateingAnyTable = true;
    this.positionOfTableUpdadte = position;

    this.selectedShape = tableDetails.tableShape;
    this.selectedOrientation = tableDetails.orientation;

    // if (tableDetails.tableShape && tableDetails.tableShape.length > 0) {
    //   tableDetails.tableShape = tableDetails.tableShape.toLowerCase();
    //   this.selectedShape = tableDetails.tableShape.charAt(0).toUpperCase() +
    //     tableDetails.tableShape.slice(1);
    // }

    // if (tableDetails.orientation && tableDetails.orientation.length > 0) {
    //   tableDetails.orientation = tableDetails.orientation.toLowerCase();
    //   this.selectedOrientation = tableDetails.orientation.charAt(0).toUpperCase() +
    //     tableDetails.orientation.slice(1);
    // }

    this.minSeat = tableDetails.minSeats;
    this.maxSeat = tableDetails.maxSeats;
    this.isMovable = tableDetails.movable;
    this.isReservationRequire = tableDetails.reservationRequired;
    this.isSmokingAllow = tableDetails.smoking;
    this.tableId = tableDetails.tableId;
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-lg' }));
  }

  addNewTableToList() {
    this.addTableObj = {
      tableShape: this.selectedShape.toUpperCase(),
      orientation: this.selectedOrientation.toUpperCase(),
      minSeats: this.minSeat,
      maxSeats: this.maxSeat,
      movable: this.isMovable,
      reservationRequired: this.isReservationRequire,
      smoking: this.isSmokingAllow,
      id: this.tableId,
      restaurantId: this.restId
    };
    if (this.isUpdateingAnyTable) {
      this.addedTableList[this.positionOfTableUpdadte] = this.addTableObj
    } else {
      this.addedTableList.push(this.addTableObj);
    }
    this.modalRef.hide();
    this.saveTableMsg = "Save Table";
    this.isUpdateingAnyTable = false;
    this.positionOfTableUpdadte = 0;
  }

  createNewRestaurant() {
    this.formSubmitted = true;
    // console.log(myForm);
    // console.log("name is " + myForm.value.restName);
    // if (true) {

    if (this.addedMealDetailList && this.addedMealDetailList.length > 0) {
      for (let index = 0; index < this.addedMealDetailList.length; index++) {
        this.addedMealDetailList[index].endDate = this.datePipe.
          transform(this.addedMealDetailList[index].endDate, "yyyy-MM-dd");
        this.addedMealDetailList[index].startDate = this.datePipe.
          transform(this.addedMealDetailList[index].startDate, "yyyy-MM-dd"); //output : Dec-12-2018
      }
    }

    let diningData = {
      "restaurantName": this.restaurantName,
      "openingTime": this.openingTime,
      "closingTime": this.closingTime,
      "restaurantDescription": this.description,

      "diningTables": this.addedTableList,
      "diningMeals": this.addedMealDetailList,
      "media": this.mediaFileList
    };

    if (this.isEdit) {
      diningData["id"] = this.restId;
    }


    console.log("from data is" + diningData);
    // this.startDate = this.datePipe.transform(this.startDate, "yyyy-MM-dd");
    // this.endDate = this.datePipe.transform(this.endDate, "yyyy-MM-dd");
    this.callSaveRestaurant(diningData);
  }

  callSaveRestaurant(diningData) {
    let url = this.isEdit ? "updateRestaurant" : "createRestaurant";

    this.httpService.call(url, diningData).subscribe((data) => {

      let displayMsg = 'Restaurant saved successfully';
      if (this.isEdit) {
        displayMsg = "Restaurant updated successfully"
      }
      this.alert = {
        type: "success",
        title: "Success",
        message: displayMsg
      };
      // this.showNotification();
      this.clearDiningData();
      this.isEdit = false;
      this.resetTab.emit();
      this.notify.emit(this.alert);

    }, (error) => {
      this.alert = {
        type: "error",
        title: "Failed",
        message: "Some error occured"
      };
      // this.showNotification();
      this.notify.emit(this.alert);
    });
  }

  showNotification() {
    this.isShowAlert = true;
    setTimeout(() => {
      this.isShowAlert = false;
    }, 3000);

  }
  clearDiningData() {
    this.restaurantName = '';
    this.openingTime = '';
    this.closingTime = '';
    this.description = '';
    this.addedMealDetailList = [];
    this.addedTableList = [];
    this.mediaFileList = [{ url: "" }];
    this.restId = null;
    this.tableId = null;
  }
  updateTimeFormat(value) {
    if (value == "openingTime") {
      this.openingTime = moment(this.openingTime).format("HH:mm");
    } else {
      this.closingTime = moment(this.closingTime).format("HH:mm");
    }
    console.log(this.openingTime);
  }


  showNewTableDialog(template) {
    this.selectedShape = "";
    this.selectedOrientation = "";
    this.minSeat = '';
    this.maxSeat = '';
    this.isMovable = false;
    this.isReservationRequire = false;
    this.isSmokingAllow = false;

    this.saveTableMsg = "Save Table";
    this.isUpdateingAnyTable = false;
    this.positionOfTableUpdadte = 0;
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-lg' }));
  }

  deleteAddedTablFromList(position) {
    this.addedTableList.splice(position, 1);
  }

  showMealComponent() {
    this.needToShowAddMealComponent = true;
  }

  showDiningScreen(event) {
    this.needToShowAddMealComponent = false;
  }

  getUpdatedMealList(mealList) {
    this.addedMealDetailList = mealList;
  }
  ////////////
  validateForm() {
    this.isDataValidate = true;

    if (!this.restaurantName || !this.restaurantName.trim().length) {
      this.isValid['isRestName'] = false;
      this.isDataValidate = false;
    }
    if (!this.openingTime || !this.openingTime.trim().length) {
      this.isValid['isOpenTime'] = false;
      this.isDataValidate = false;
    }
    if (!this.closingTime || !this.closingTime.trim().length) {
      this.isValid['isCloseTime'] = false;
      this.isDataValidate = false;
    }

    if (!this.totalCapacity || !this.totalCapacity.trim().length) {
      this.isValid['isCapacity'] = false;
      this.isDataValidate = false;
    }
    // if (!this.price || !this.price.trim().length) {
    //   this.isValid['isPrice'] = false;
    //   this.isDataValidate = false;
    // }
    if (!this.cancellationFee || !this.cancellationFee.trim().length) {
      this.isValid['isCancellationFee'] = false;
      this.isDataValidate = false;
    }

    return this.isDataValidate;
  }


  getAddedMealDetails(mealDetailsList: any) {
    console.log("from add new dining component");
    console.log(mealDetailsList);
    this.addedMealDetailList = mealDetailsList;
  }

  validateRestName() {
    if (!this.restaurantName || !this.restaurantName.trim().length) {
      this.isValid['isRestName'] = false;
      return false;
    } else {
      return true;
    }
  }
  validateOpenTime() {
    if (!this.openingTime || !this.openingTime.trim().length) {
      this.isValid['isOpenTime'] = false;
      return false;
    } else {
      return true;
    }
  }

  validateCloseTime() {
    if (!this.closingTime || !this.closingTime.trim().length) {
      this.isValid['isCloseTime'] = false;
      return false;
    } else {
      return true;
    }
  }

  validateTotalCapacity() {
    if (!this.totalCapacity || !this.totalCapacity.trim().length) {
      this.isValid['isCapacity'] = false;
      return false;
    } else {
      return true;
    }
  }

  validatePrice() {
    if (!this.price || !this.price.trim().length) {
      this.isValid['isPrice'] = false;
      return false;
    } else {
      return true;
    }
  }

  validateCancellationFee() {
    if (!this.cancellationFee || !this.cancellationFee.trim().length) {
      this.isValid['isCancellationFee'] = false;
      return false;
    } else {
      return true;
    }
  }
  counter = 1;
  addMediaFile() {
    this.counter = this.counter + 1;

    let newMediaFile = {
      url: "",
      index: this.counter
    };
    this.mediaFileList.push(newMediaFile);
    console.log(this.mediaFileList);
  }

  removeMediaFile(i) {
    if (this.mediaFileList.length > 1) {
      this.mediaFileList.splice(i, 1);
    }
  }

  // setCheckboxSelection(event, id) {
  //   if (event.target.checked) {
  //     this.selectedExcursion.push(id);
  //   } else {
  //     this.selectedExcursion.splice(this.selectedExcursion.indexOf(id));
  //   }
  //   console.log(this.selectedExcursion);
  // }
}


