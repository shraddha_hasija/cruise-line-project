import { Component, OnInit, ViewChild } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap';

@Component({
  selector: 'app-dining',
  templateUrl: './dining.component.html',
  styleUrls: ['./dining.component.css']
})
export class DiningComponent implements OnInit {

  @ViewChild('diningTabs') diningTabs: TabsetComponent;

  isShowAlert: boolean = false;
  isRefreshList: boolean = false;
  alert = {
    message: '',
    type: "",
    title: ""
  };


  updateDiningData = {};
  constructor() { }

  ngOnInit() {
  }

  changeToCreateDiningTab(tabId) {
    this.diningTabs.tabs[tabId].active = true;
    this.diningTabs.tabs[tabId].heading = "Edit Dining";
  }

  gotoEditDining(diningData) {
    this.isRefreshList = false;
    this.changeToCreateDiningTab(1);
    this.updateDiningData = diningData;
  }

  resetTab() {
    this.diningTabs.tabs[1].heading = "New Excursion";
    this.diningTabs.tabs[0].active = true;
    this.isRefreshList = true;
    // this.resetExcursion(0);
    // this.selectTab(1)
  }

  selectTab(tab) {
    if (tab === 1) {
      this.isRefreshList = true;
    } else {
      this.isRefreshList = false;
    }
  }

  showNotification(msgObj) {
    this.isShowAlert = true;
    this.alert = msgObj;

    setTimeout(() => {
      this.isShowAlert = false;
    }, 3000);
  }

  resetExcursion(tabId) { //not in use as of now
    if (tabId == 1) {
      this.diningTabs.tabs[tabId].heading = "New Excursion";
    }
    this.diningTabs.tabs[tabId].active = true;
  }
}
