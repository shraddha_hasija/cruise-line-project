import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule, APP_BASE_HREF, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { BsDropdownModule, BsDatepickerModule, TimepickerModule, TabsModule, AlertModule, ModalModule } from 'ngx-bootstrap';

import { SharedModule } from "./shared/shared.module";

import { AppComponent } from './app.component';
import { appRoutes } from "./app.routes";
import { AppHeaderComponent } from './app-header/app-header.component';
import { DashboardComponent } from './dashboard/dashboard.component';



@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    DashboardComponent,



  ],
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    TimepickerModule.forRoot(),
    NgxPaginationModule,
    SharedModule,
    TabsModule.forRoot(),
    AlertModule.forRoot(),
    appRoutes
  ],
  providers: [{ provide: APP_BASE_HREF, useValue: "/cruiseline" }, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
