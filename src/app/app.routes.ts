/* This TS is used for configuring various application routes.  */
import { RouterModule, Routes } from "@angular/router";
import { DashboardComponent } from "./dashboard/dashboard.component";

const routeConfig: Routes = [
    {
        path: 'home',
        component: DashboardComponent
    },
    {
        path: 'excursion',
        loadChildren: '../app/excursion/excursion.module#ExcursionModule'
    },
    {
        path: 'dining',
        loadChildren: '../app/dining/dining.module#DiningModule'
    },
    {
        path:'discount',
        loadChildren:'../app/discount/discount.module#DiscountModule'
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: 'home'
    }
];

export const appRoutes = RouterModule.forRoot(routeConfig, { useHash: true, onSameUrlNavigation: 'reload' });