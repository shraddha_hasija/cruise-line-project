import { Component, OnInit } from '@angular/core';

import { HTTPService } from "./shared/services/http.service";
import { URLService } from "./shared/services/url.service";
import { environment } from "../environments/environment";
import { SharedDataService } from './shared/services/shared-data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'app';
  isShowAlert = false;
  alert = {
    message: '',
    type: "",
    title: ""
  };
  notificationObservable: Observable<any>;

  constructor(private httpService: HTTPService, private sharedService: SharedDataService) {
    this.notificationObservable = this.sharedService.subscribeNotifications();
    this.notificationObservable.subscribe((data) => {
      this.alert = data;
      this.isShowAlert = true;

      setTimeout(() => {
        this.isShowAlert = false;
      }, 10000);
    });
  }

  ngOnInit() {
    //this.testService();
  }

  testService() {
    this.httpService.call('login', null).subscribe((data) => {
      console.log(data);
    });
  }

  ngOnDestroy() {
    //this.notificationObservable
  }
}
