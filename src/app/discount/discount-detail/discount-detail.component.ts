import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-discount-detail',
  templateUrl: './discount-detail.component.html',
  styleUrls: ['./discount-detail.component.css']
})
export class DiscountDetailComponent implements OnInit {
  activeSelect=false
  isDiscountCodeGenerate:boolean=true;
  currentSelected="first"
  discountRuleName:string='';
  description='';
  Guest='';
  guestName='';
  isDiscountCode:boolean=false;
  discountCode='';
  discountAmount='';
  categoryOfGuest='';
  constructor() { }
  isStep1Active:boolean=true;
  isStep2Active:boolean=false;
  isStep3Active:boolean=false;
  isDiscountProduct:boolean=false;
  isAllowMultiple:boolean=false;
  excludedName=''
  selectedStatus='Active';
  statusGroup:any=["Active","InActive"];
  selectedCategory='Gold';
  guestCategory:any=["Gold","Platinum","Diamond"];
  selectedCurrencyType="$"
  currencyList:any=["$","%"]
  fromDate=new Date();
  toDate=new Date();
  
  ngOnInit() {
  }
  onStatusChange(status){
    this.selectedStatus=status;
  }
  onCategoryChange(category){
    this.selectedCategory=category;
  }
  onCurrencyChange(currency){
    this.selectedCurrencyType=currency;
  }
  selecteDiscountCodeGenerate(){
    this.isDiscountCodeGenerate=!this.isDiscountCodeGenerate;
  }
  saveDiscountDetail(form){
    console.log(this.discountRuleName);

    console.log(this.toDate)
   
  }
  selectButton1(){
    if(this.currentSelected== "first" && this.activeSelect==false)
    {
    this.currentSelected="second";
  }
 else if(this.currentSelected=="second"){
  this.currentSelected="third";
 }
}
selectButton2(){
  if(this.currentSelected=="second"){
    this.currentSelected ="first"
  }
  else {
    this.currentSelected ="second"
  }
}

}