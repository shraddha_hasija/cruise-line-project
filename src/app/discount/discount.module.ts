import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import {  TabsModule,BsDatepickerModule,BsDropdownModule} from "ngx-bootstrap";
import {DiscountComponent} from './discount.component';
import {discountRoutes} from './discount-routing.module';
import { DiscountListComponent } from './discount-list/discount-list.component';
import { DiscountDetailComponent } from './discount-detail/discount-detail.component'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    discountRoutes,
    TabsModule.forRoot(),
    BsDatepickerModule,
    BsDropdownModule
  ],
  declarations: [DiscountComponent, DiscountListComponent, DiscountDetailComponent]
})
export class DiscountModule { }
