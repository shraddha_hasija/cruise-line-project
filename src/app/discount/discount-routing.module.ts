import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import {DiscountComponent} from './discount.component';

const routerConfig: Routes = [
  {
      path: 'list',
      component: DiscountComponent
  },
  {
      path: '',
      redirectTo: 'list'
  },
  
];

export const discountRoutes = RouterModule.forChild(routerConfig);