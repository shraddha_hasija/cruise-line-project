import { TabsetComponent } from 'ngx-bootstrap';
import { Component, OnInit,ViewChild } from '@angular/core';

@Component({
  selector: 'app-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.css']
})
export class DiscountComponent implements OnInit {

  @ViewChild('discountTabs') excursionTabs: TabsetComponent;
  listTabActive = false;
  isShowAlert: boolean = false;
  isRefreshList: boolean = false;
  constructor() { }

  ngOnInit() {
  }
  selectTab(tab) {
    if (tab === 1) {
      this.isRefreshList = true;
      this.listTabActive = true;
    } else {
      this.isRefreshList = false;
      this.listTabActive = false;
    }
  }

}
