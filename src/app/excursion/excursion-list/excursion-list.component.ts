import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';
import { CommonUtilityService } from 'src/app/shared/services/common-utility.service';
import { HTTPService } from 'src/app/shared/services/http.service';
import { URLService } from 'src/app/shared/services/url.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-excursion-list',
  templateUrl: './excursion-list.component.html',
  styleUrls: ['./excursion-list.component.css']
})
export class ExcursionListComponent implements OnInit {

  @Output() notify: EventEmitter<any> = new EventEmitter();
  @Output() edit: EventEmitter<any> = new EventEmitter();

  @Input() reloadList: boolean = false;
  @Input() isAuthenticated: boolean = false;
  isShowAlert:boolean=false
  applyStatus:boolean=false;
  stetusExcursionlist:any=[];
  isNameSelected:boolean=true;
  isIdSelected:boolean=true;
  isStartDateSelected:boolean=true;
  isEndDateSelected:boolean=true;
  
  title:any='';
  modalRef: BsModalRef;
  excursionList: any = [];
  excursionListSoruce: any = [];

  actionsArr = ["ACTIVE", "INACTIVE"];
  selectedAction: string = "INACTIVE";
  selectedExcursion = [];
  noOfItems = 10;
  currPage = 1;
  showSelected = [];
  alert = {
    message: '',
    type: "",
    title: ""
  };

  isShowFilterBox = true;
  filterType = "";
  searchByName: any;
  searchById: any;
  searchByStartDate: any;
  searchByEndDate: any;
  showDatevalidationMsg = false;
  isNameDesc = false;
  isIDDesc = false;
  isVendorDesc = false;
  isPortDesc = false;
  dpConfig = {
    dateInputFormat: 'MMM DD YYYY',
    containerClass: 'theme-red' 
  };
  selectedBookingItemId=0;

  constructor(private commonService: CommonUtilityService, private httpService: HTTPService,
    private urlService: URLService, private modalService: BsModalService) { }

  ngOnInit() {
    //this.getAuthToken();
    //this.getExcursions();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.reloadList && changes.reloadList.currentValue && !changes.reloadList.isFirstChange()) {
      this.refresh();
    }

    if (changes.isAuthenticated && changes.isAuthenticated.currentValue) {
      this.getExcursions();
    }
  }

  getAuthToken() {
    let req = {
      "username": "user",
      "password": "user"
    };
    this.httpService.call("authenticate", req).subscribe((data) => {
      console.log(data);
    });
  }

  sortData(order, key) {
    let dataArr = JSON.parse(JSON.stringify(this.excursionList));
    if (order === "asc") {
      this.excursionList = this.commonService.sortDataAsc(key, dataArr);
    }
    else if (order === "desc") {
      this.excursionList = this.commonService.sortDataDesc(key, dataArr);
    }
  }

  refresh() {
    this.getExcursions();
  }

  getExcursions() {
    this.httpService.call("getExcursion").subscribe((data) => {
      this.excursionList = data;
      this.excursionListSoruce = data;
    }, (error) => {
      this.alert = {
        type: "error",
        message: 'Some error occured',
        title: "Failed"
      };
      this.notify.emit(this.alert);
    });
  }

  setCheckboxSelection(event, id, row) {
    if (event.target.checked) {
      this.selectedExcursion.push(id);
      if(this.applyStatus == true){
        row["status"]=this.selectedAction;
      }
      console.log(row);
    } else {
      this.selectedExcursion.splice(this.selectedExcursion.indexOf(id));
    }
    console.log(this.selectedExcursion);
  }

  changeAllCheckboxSelection(event) {
    if (event.target.checked) {
      for (let i = 0; i < this.excursionList.length; i++) {
        this.excursionList[i].id ? this.selectedExcursion.push(this.excursionList[i].id) : null;
        this.showSelected[i] = true;
      }
    } else {
      this.selectedExcursion = [];
      this.showSelected = [];
    }
    console.log(this.selectedExcursion);
    console.log(this.showSelected);
  }

  applySelection() {
   this.applyStatus=true
   
    // console.log(this.selectedAction);
    console.log(this.selectedExcursion)
    // this.excursionList.filter(obj =>{
    //   if(obj.status==this.selectedAction){
    //    this.stetusExcursionlist.push(obj);
    //    this.excursionList= this.stetusExcursionlist;
    //      console.log(this.stetusExcursionlist);
    //   }
    // })
    
   
    
  }

  onSelectionChange(value) {
    console.log(value);
    this.selectedAction = value;
  }

  editExcursion(row) {
    this.edit.emit(row);
  }

  filterSelection(template) {
    this.title='';
    this.filterType='';
    this.isEndDateSelected=true;
    this.isStartDateSelected=true;
    this.isIdSelected=true;
    this.isNameSelected=true;
    this.modalRef = this.modalService.show(template);
    
    // Object.assign({}, this.excursionList);
  }

  filterData() {
    let dataArr = JSON.parse(JSON.stringify(this.excursionListSoruce));
    let isDateCheck=false
    // let listCopy = Object.assign([], this.excursionList);
    switch (this.filterType) {
      case 'byName':
        this.excursionList = this.commonService.filterByKey(this.searchByName, dataArr, "name");
        this.modalRef.hide();

        break;
      case 'byId':
        this.excursionList = this.commonService.filterById(this.searchById, dataArr, "id");
        this.modalRef.hide();

        break;
      case 'byDate':
        if (!this.searchByStartDate ||
          !this.searchByEndDate ||
          +this.searchByStartDate > +this.searchByEndDate) {
          isDateCheck=true;
          this.showDatevalidationMsg = true;
          this.title='Please Add Appropriate Dates';
        } else {
          this.modalRef.hide();
          this.showDatevalidationMsg = false;
          this.excursionList = this.commonService.filterByDates(this.searchByStartDate, this.searchByEndDate
            , dataArr);
        }
        break;
    } 
    let alertMessage = "";
    if(isDateCheck==false){
    if(this.excursionList.length==0){
      alertMessage = 'No Records Found'
    }else{
      alertMessage = 'Search result'
    }
  
    this.alert = {
      type: "alert",
      message: alertMessage,
      title: ''
    };
    this.notify.emit(this.alert);
  }
    // this.showNotification
    this.searchByName='';
    this.searchById='';
    this.searchByEndDate='';
    this.searchByStartDate='';
    this.isEndDateSelected=true;
    this.isStartDateSelected=true;
    this.isIdSelected=true;
    this.isNameSelected=true;
    this.filterType='';
    // if(this.showDatevalidationMsg==true){
      // this.isEndDateSelected=false;
      // this.isStartDateSelected=false;
    //   this.filterType='byDate';
    // }
    
    
    
    console.log(this.excursionList);
  }
  
  showNotification() {
    this.isShowAlert = true;
    setTimeout(() => {
      this.isShowAlert = false;
    }, 3000);

  }
  clearFilter() {
    this.excursionList = this.excursionListSoruce;
  }

  deleteSelectedExcursion() {
    let url = this.urlService.getAPIData('deleteExcursion');
    console.log(url);
    this.httpService.call(url, this.selectedBookingItemId).subscribe((data) => {
      this.alert = {
        type: "success",
        title: "Success",
        message: "Excursion deleted successfully"
      }
      this.modalRef.hide();
      this.notify.emit(this.alert);
      this.refresh();
    }, (error) => {
      this.alert = {
        type: "error",
        title: "Failed",
        message: "Some error occured"
      };
      this.modalRef.hide();
      this.notify.emit(this.alert);
    });

  }

  showConfirmDeleteDialog(id, template) {
    this.modalRef = this.modalService.show(template,Object.assign({}, { class: 'modal-md'}));
    this.selectedBookingItemId=id;
  }
  searchedByName(){
   this.searchById='';
   this.searchByStartDate='';
   this.searchByEndDate='';
   this.isNameSelected=false;
   this.isIdSelected=true
   this.isEndDateSelected=true;
    this.isStartDateSelected=true;
    // this.filterType="byName"
  
  }
  searchedById(){
    this.searchByName='';
    this.searchByStartDate='';
    this.searchByEndDate='';
    this.isNameSelected=true;
    this.isIdSelected=false
    this.isEndDateSelected=true;
    this.isStartDateSelected=true;
  }
  searchedByDate(){
    this.searchByName='';
    this.searchById='';
    this.isNameSelected=true;
    this.isIdSelected=true;
    this.isEndDateSelected=false;
    this.isStartDateSelected=false;
  }
}
