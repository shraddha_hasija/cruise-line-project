import { RouterModule, Routes } from "@angular/router";
import { ExcursionComponent } from "./excursion.component";
import { NewExcursionComponent } from "./new-excursion/new-excursion.component";

const routerConfig: Routes = [
    {
        path: 'list',
        component: ExcursionComponent
    },
    {
        path: '',
        redirectTo: 'list'
    },
    {
        path: 'add',
        component: NewExcursionComponent
    },
    {
        path: 'book',
        loadChildren: '../excursion/book-excursion/book-excursion.module#BookExcursionModule'
    }
];

export const excursionRoutes = RouterModule.forChild(routerConfig);