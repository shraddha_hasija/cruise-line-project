import { NgModule } from "@angular/core";
import { ExcursionComponent } from "./excursion.component";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { excursionRoutes } from "./excursion-routing.module";
import { NewExcursionComponent } from './new-excursion/new-excursion.component';
import { NgxPaginationModule } from "ngx-pagination";
import { BsDropdownModule, BsDatepickerModule, TimepickerModule, TabsModule, ModalModule } from "ngx-bootstrap";
import { NgxEditorModule } from 'ngx-editor';
import { SharedModule } from "../shared/shared.module";
import { ExcursionListComponent } from './excursion-list/excursion-list.component';

@NgModule({
    declarations: [ExcursionComponent, NewExcursionComponent, ExcursionListComponent],
    imports: [
        FormsModule,
        CommonModule,
        SharedModule,
        BsDropdownModule.forRoot(),
        NgxPaginationModule,
        NgxEditorModule,
        excursionRoutes,
        BsDatepickerModule,
        TimepickerModule,
        TabsModule.forRoot(),
        ModalModule
    ],
    exports: [],
    entryComponents: []
})
export class ExcursionModule {

}