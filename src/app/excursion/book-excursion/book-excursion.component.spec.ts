import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookExcursionComponent } from './book-excursion.component';

describe('BookExcursionComponent', () => {
  let component: BookExcursionComponent;
  let fixture: ComponentFixture<BookExcursionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookExcursionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookExcursionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
