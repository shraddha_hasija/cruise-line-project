import { RouterModule, Routes } from "@angular/router";
import { BookExcursionComponent } from "./book-excursion.component";
import { SearchExcursionComponent } from "./search-excursion/search-excursion.component";
import { BookListComponent } from "./book-list/book-list.component";
import { BookExcursionAuthGuard } from "src/app/shared/guards/book-excursion-auth.guard";
import { BookingDetailsComponent } from "./booking-details/booking-details.component";

const routerConfig: Routes = [
    {
        path: 'new',
        component: BookExcursionComponent
    },
    {
        path: 'search',
        component: SearchExcursionComponent,
        // canActivate: [BookExcursionAuthGuard]
    },
    {
        path: 'list',
        component: BookListComponent
    },
    {
        path: 'details',
        component: BookingDetailsComponent
    },

    {
        path: '',
        redirectTo: 'new'
    },
     {
        path: '',
        redirectTo: 'list'
    },
    {
        path: '',
        redirectTo: 'details'
    }
];

export const bookExcursionRoutes = RouterModule.forChild(routerConfig);