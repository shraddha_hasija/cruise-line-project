import { Component, OnInit } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap';
import { HTTPService } from 'src/app/shared/services/http.service';
import { URLService } from 'src/app/shared/services/url.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-booking-detail-edited',
  templateUrl: './booking-detail-edited.component.html',
  styleUrls: ['./booking-detail-edited.component.css']
})
export class BookingDetailEditedComponent implements OnInit {
  bookingList: any = [];
  list = [];
  bookingDetailsObj = [];
  bookedTravellerList: any = [];

  constructor(public bsModalRef: BsModalRef, private httpService: HTTPService,
    private urlService: URLService, private router: Router) { }

  ngOnInit() {

    this.bookingDetailsObj = this.list[0] ? this.list[0] : {};
    this.bookedTravellerList = this.bookingDetailsObj ? this.bookingDetailsObj['bookingUserPrices'] : '';
    console.log('listdetail', this.bookingDetailsObj['bookingUserPrices']);
  }

  deleteTraveller(no) {
    if (this.bookedTravellerList.length > 1) {
      this.bookedTravellerList.splice(no, 1);
      console.log(this.bookedTravellerList);
    }
  }

  updateBooking() {
    let url = "updateBookedExcursion";
    this.bookingDetailsObj["noOfTickets"] = this.bookedTravellerList.length;
    // this.bookingDetailsObj["totalAmount"]=
    this.bookingDetailsObj['bookingUserPrices'] = this.bookedTravellerList;
    console.log(this.bookingDetailsObj);
    console.log(this.bookedTravellerList);

    this.httpService.call(url, this.bookingDetailsObj).subscribe((data) => {
      //   // this.alert = {
      //   //   type: "success",
      //   //   title: "Success",
      //   //   message: "Excursion saved successfully"
      //   // };
      console.log("upDated Successfully");
      this.bsModalRef.hide();
      this.router.navigate(['/excursion/book/list']);

    }, (error) => {
      console.log("something went wrong");
      //   // this.alert = {
      //   //   type: "error",
      //   //   title: "Failed",
      //   //   message: "Some error occured"
      //   // };
      //   // this.notify.emit(this.alert);
    });
  }

}
