import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingDetailEditedComponent } from './booking-detail-edited.component';

describe('BookingDetailEditedComponent', () => {
  let component: BookingDetailEditedComponent;
  let fixture: ComponentFixture<BookingDetailEditedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingDetailEditedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingDetailEditedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
