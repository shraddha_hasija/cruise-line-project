import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from 'src/app/shared/services/common-utility.service';
import { HTTPService } from 'src/app/shared/services/http.service';
import { URLService } from 'src/app/shared/services/url.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-look-up-book',
  templateUrl: './look-up-book.component.html',
  styleUrls: ['./look-up-book.component.css']
})
export class LookUpBookComponent implements OnInit {
  isShowAlert:boolean=false;
  alert = {
    message: '',
    type: "",
    title: ""
  };
  errorCabinMessage='';
  errorTravellerMessage='';
  cabin_selected=false;
  selectCabinNo:boolean=false;
  selectTravellerName:boolean=true;
  scanBy = "cabinNumber";
  includeTravellingWith = false;
  cabinNumber = "";
  nameOftraveller = "";
  travellerList: any = [];
  searchedTravellerList: any = [];

  selectedTravellerList: any = [];
  selectedItemPosition: any;
  selectAllChecked = false;


  constructor(private commonService: CommonUtilityService, private httpService: HTTPService,
    private urlService: URLService, private sharedService: SharedDataService, private router: Router) { }

  ngOnInit() {
    this.selectedTravellerList = [];
  }

  searchTraveller() {
    if(!this.cabinNumber && !this.selectCabinNo){
      this.errorCabinMessage ="Please enter cabin number"
    }
    else if(!this.nameOftraveller && !this.selectTravellerName){
      this.errorTravellerMessage="Please enter name of traveller"
    }
    else{
    this.selectAllChecked = false;
    this.httpService.call("getTravellerList").subscribe((data) => {
      this.travellerList = data;
      this.searchedTravellerList = data;
     

      let req = {};
      req["cabinNo"] = this.cabinNumber;
      req["scanBy"] = this.scanBy;
      req["name"] = this.nameOftraveller.trim();

      console.log(req);
     
      if (this.scanBy === "cabinNumber") {
        this.searchedTravellerList = this.commonService.filterById(this.cabinNumber, this.travellerList, "cabin");

      } else {
        this.searchedTravellerList = this.filterByName(this.nameOftraveller, this.travellerList);
      }
      if (this.searchedTravellerList.length==0) {
        this.alert = {
          type: "error",
          message: 'No Records Found',
          title: ""
        };
        this.showNotification();
        // this.sharedService.notify(alert);
      }
     
    }, (error) => {
      let alert = {
        type: "error",
        message: 'Some error occured',
        title: "Failed"
      };
      this.sharedService.notify(alert);
    });
    this.errorCabinMessage='';
    this.errorTravellerMessage='';

  }
}
showNotification() {
  this.isShowAlert = true;
  setTimeout(() => {
    this.isShowAlert = false;
  }, 3000);

}

  filterByName(searchValue: any, data: any): any {
    let listData = data;
    let term = searchValue && searchValue.length ? searchValue.toLowerCase() : "";
    listData = listData.filter(obj => {
      let val = obj["firstName"] || obj["lastName"] ? (obj["firstName"].toLowerCase() || obj["firstName"]) + " " + (obj["lastName"].toLowerCase() || obj["lastName"]) : "";
      return val.indexOf(term) > -1;
    });
    console.log(listData);
    return listData;
  }

  addSelectedTraveller() {
    // this.selectedTravellerList = [];
    console.log(this.searchedTravellerList);
    for (let i = 0; i < this.searchedTravellerList.length; i++) {
      console.log(this.selectedTravellerList.some(x => x.id === this.searchedTravellerList[i].id));
      if (this.searchedTravellerList[i].checked &&
        !this.selectedTravellerList.some(x => x.id === this.searchedTravellerList[i].id)) {
        this.selectedTravellerList.push(this.searchedTravellerList[i]);
      }
    }
  }

  travellerSelectionChange(event, position) {
    this.searchedTravellerList[position].checked = event.target.checked;
  }

  bookExcursion() {
    console.log(this.selectedTravellerList);
    this.sharedService.setExcursionUserData(this.selectedTravellerList);
    this.router.navigate(['/excursion/book/search']);
  }

  deleteTraveller(index: number) {
    // console.log(index);
    this.selectedTravellerList.splice(index, 1);
  }

  changeAllCheckboxSelection(event) {
    for (let i = 0; i < this.searchedTravellerList.length; i++) {
      this.searchedTravellerList[i].checked = event.target.checked;
    }
  }
  SelectCabinNo(){
   this.selectCabinNo=false;
   this.selectTravellerName=true;
   this.nameOftraveller=''
   this.cabin_selected=true
   this.errorTravellerMessage='';
  //  this.scanBy=''
  }
  TravellerNameSelected(){
    this.selectTravellerName=false;
    this.selectCabinNo=true;
    this.cabinNumber=''
    this.errorCabinMessage=''
  }
}
