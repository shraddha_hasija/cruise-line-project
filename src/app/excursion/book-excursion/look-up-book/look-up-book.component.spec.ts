import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LookUpBookComponent } from './look-up-book.component';

describe('LookUpBookComponent', () => {
  let component: LookUpBookComponent;
  let fixture: ComponentFixture<LookUpBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LookUpBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LookUpBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
