import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from 'src/app/shared/services/common-utility.service';
import { HTTPService } from 'src/app/shared/services/http.service';
import { URLService } from 'src/app/shared/services/url.service';
import { SharedDataService } from 'src/app/shared/services/shared-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scan-book',
  templateUrl: './scan-book.component.html',
  styleUrls: ['./scan-book.component.css']
})
export class ScanBookComponent implements OnInit {

  searchedTravellerList: any = [];

  constructor(private commonService: CommonUtilityService, private httpService: HTTPService,
    private urlService: URLService, private sharedService: SharedDataService, private router: Router) { }

  ngOnInit() {
    this.searchTraveller();
  }

  searchTraveller() {
    // here we get result and give it to
    // searchedTravellerList
    // this.httpService.call("getTravellerList").subscribe((data) => {
    //   this.searchedTravellerList = data;
    // }, (error) => {
    //   let alert = {
    //     type: "error",
    //     message: 'Some error occured',
    //     title: "Failed"
    //   };
    //   this.sharedService.notify(alert);
    // });
  }

  deleteTraveller(index: number) {
    // console.log(index);
    this.searchedTravellerList.splice(index, 1);
  }

  bookExcursion() {
    // this.sharedService.setExcursionUserData(this.searchedTravellerList);
    // this.router.navigate(['/excursion/book/search']);
  }
}
