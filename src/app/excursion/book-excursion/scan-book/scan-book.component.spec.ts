import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanBookComponent } from './scan-book.component';

describe('ScanBookComponent', () => {
  let component: ScanBookComponent;
  let fixture: ComponentFixture<ScanBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScanBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScanBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
