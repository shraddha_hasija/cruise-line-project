import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { SharedModule } from "src/app/shared/shared.module";
import { BookExcursionComponent } from "./book-excursion.component";
import { NgModule } from "@angular/core";
import { bookExcursionRoutes } from "./book-excursion.routes";
import { SearchExcursionComponent } from './search-excursion/search-excursion.component';
import { BsDatepickerModule, BsDropdownModule, ModalModule } from "ngx-bootstrap";
import { TabsModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from "ngx-pagination";
import { ScanBookComponent } from './scan-book/scan-book.component';
import { LookUpBookComponent } from './look-up-book/look-up-book.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookingDetailsModalComponent } from './booking-details-modal/booking-details-modal.component';
import { BookingDetailsComponent } from './booking-details/booking-details.component';
import {BookingDetailEditedComponent} from './booking-detail-edited/booking-detail-edited.component'

@NgModule({
    declarations: [BookExcursionComponent, ScanBookComponent, LookUpBookComponent,
        SearchExcursionComponent, BookListComponent, BookingDetailsModalComponent, BookingDetailsComponent,BookingDetailEditedComponent],
    imports: [
        bookExcursionRoutes,
        FormsModule,
        CommonModule,
        SharedModule,
        BsDatepickerModule,
        BsDropdownModule,
        NgxPaginationModule,
        TabsModule,
        ModalModule.forRoot()
    ],
    exports: [],
    entryComponents: [BookingDetailsModalComponent,BookingDetailEditedComponent]
})
export class BookExcursionModule {

}