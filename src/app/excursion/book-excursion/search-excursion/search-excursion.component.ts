import { Component, OnInit, ɵConsole } from '@angular/core';
import { HTTPService } from 'src/app/shared/services/http.service';
import { URLService } from 'src/app/shared/services/url.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { BookingDetailsModalComponent } from '../booking-details-modal/booking-details-modal.component';
import { SharedDataService } from 'src/app/shared/services/shared-data.service';
import { CommonUtilityService } from 'src/app/shared/services/common-utility.service';

declare const require: any;

@Component({
  selector: 'app-search-excursion',
  templateUrl: './search-excursion.component.html',
  styleUrls: ['./search-excursion.component.css']
})
export class SearchExcursionComponent implements OnInit {

  // staticList = require('../../../../assets/mock-data/excursionList.json');
  select_today: boolean = true;
  select_tomorrow: boolean = false;
  selectedDate: any = '';
  isDisabled = false;
  // is_date_change=false;
  readonly DEFAULT_CATEGORY = "All";
  dpConfig = {
    dateInputFormat: 'MMM DD YYYY',
    containerClass: 'theme-red'
  };

  excursionName = "";
  dateVal: any = new Date();
  categoryList = ["All", "Adventure", "Club Mariner", "Concierge Choice", "Diving", "Hiking", "Refreshments", "Seated", "Special Event", "Walking", "Water", "Wheelchair"];
  selectedCategory = "";
  travellerData = [];
  searchExcursionList = [];
  filterExcursionList = [];
  bsModalRef: BsModalRef;
  availablityOfSeat = 0;
  select_specific_date = false;
  alert = {
    message: '',
    type: "",
    title: ""
  };
  isShowAlert = false;

  constructor(private httpService: HTTPService, private urlService: URLService,
    private bsModalService: BsModalService, private sharedService: SharedDataService,
    private commonService: CommonUtilityService) { }

  ngOnInit() {
    this.selectedCategory = this.DEFAULT_CATEGORY;
    this.getTravellerData();

  }

  onCategoryChange(cat) {
    this.selectedCategory = cat;
  }

  selectToday() {
    this.dateVal = new Date();
    this.select_today = true;
    this.select_tomorrow = false;
    this.select_specific_date = false;
    this.dateVal.getMilliseconds();
    let tomorowDate = +this.dateVal + 86400000;
    // console.log(tomorowDate);
    // console.log(+tomorowDate);

  }
  addDays(val, num) { }
  selectTomm() {
    let dateObj = new Date();
    // let date=dateObj.getMilliseconds();
    let tomorowDate = +dateObj + 86400000;
    console.log(+tomorowDate);
    this.select_specific_date = false;

    this.dateVal = tomorowDate;
    this.select_today = false;
    this.select_tomorrow = true;
  }

  getTravellerData() {
    this.travellerData = this.sharedService.getExcursionUserData();
  }

  searchExcursion() {
    let data = {};
    if (this.select_specific_date) {
    this.dateVal = this.selectedDate;   
     console.log(this.dateVal) 
    }

    // data["name"] = this.excursionName;
    // data["date"] = +this.dateVal;
    // data["cruiseType"] = this.selectedCategory;

    // console.log(data);
  
    

    let choooseCategory = this.selectedCategory;
    if (this.selectedCategory === "All") {
      choooseCategory = "";
    }
    this.dateVal = +this.dateVal;
    let queryUrl = this.excursionName && this.excursionName.length ? "?name=" + this.excursionName : "";
    queryUrl += queryUrl && queryUrl.length ? "&" : "?";
    queryUrl += "category=" + choooseCategory;
    queryUrl += queryUrl && queryUrl.length ? "&" : "?";
    queryUrl += "queryDate=" + this.dateVal;

    this.httpService.call("searchExcursion", queryUrl).subscribe((data: any) => {
      this.searchExcursionList = data;
      this.filterExcursionList = data;
      let alertMessage = "";
      let title = "";

      if (this.filterExcursionList.length == 0) {
        alertMessage = 'Searched Excursion not available';
        title = "Sorry"
      } else {
        title = "Search Result"
        alertMessage = data.length + ' record(s) found'
      }
      this.alert = {
        type: "alert",
        message: alertMessage,
        title: title
      };
      this.showNotification();

      console.log(this.filterExcursionList)

      // let totalSeat=this.searchExcursionList['capacity']['max'];
      // this.availablityOfSeat=totalSeat-this.searchExcursionList['capacity'].bookingCount;
      // console.log(this.availablityOfSeat)
    }, (error) => {
      this.alert = {
        type: "Error",
        message: 'Something Went Wrong',
        title: "Error"
      };
      this.showNotification();

    });
  }

  bookExcursion(excursion) {
    let totalSeat = excursion["capacity"].max;
    let availableSeat = totalSeat - excursion.bookingCount;

    if (this.travellerData.length <= availableSeat) {
      const dataObj = Object.assign({}, excursion);
      dataObj['users'] = this.travellerData;
      const initialState = {
        list: [dataObj],
        date:this.dateVal,
        title: "Confirm booking"
      };
      this.bsModalRef = this.bsModalService.show(BookingDetailsModalComponent, {
        initialState,
        class: 'modal-lg'
      });
      this.bsModalRef.content.closeBtnName = 'Cancel';
    } else {
      this.alert = {
        type: "Sorry",
        message: this.travellerData.length + ' seat(s) not available',
        title: "Sorry"
      };
      this.showNotification();
    }
  }

  getExcursionRules(excursion) {
    let rulesArr = [],
      rules = excursion.rules;

    // if (rules.oneTicketPerPerson) {
    //   rulesArr.push("Only one ticket per person");
    // }
    // if (rules.applyPromotion) {
    //   rulesArr.push("Promotions allowed");
    // } else {
    //   rulesArr.push("Promotions not allowed");
    // }
    if (rules.weight) {
      rulesArr.push((rules.weight.type || "") + " weight: " + (rules.weight.value || 0));
    }
    if (rules.height) {
      rulesArr.push((rules.height.type || "") + " height: " + (rules.height.value || 0));
    }
    if (rules.age) {
      rulesArr.push((rules.age.type || "") + " age: " + (rules.age.value || 0));
    }
    if (rules.allowEmbarkingGuests) {
      rulesArr.push("Embarking guests allowed");
    } else {
      rulesArr.push("Embarking guests not allowed");
    }
    if (rules.allowInTransitGuests) {
      rulesArr.push("In-transit guests allowed");
    } else {
      rulesArr.push("In-transit guests not allowed");
    }
    if (rules.allowDisembarkingGuests) {
      rulesArr.push("Disembarking guests allowed");
    } else {
      rulesArr.push("Disembarking guests not allowed");
    }

    // if (rules.weight) {
    //   rulesArr.push((rules.weight.type || "") + " weight: " + (rules.weight.value || 0));
    // }
    // if (rules.height) {
    //   rulesArr.push((rules.height.type || "") + " height: " + (rules.height.value || 0));
    // }
    // if (rules.age) {
    //   rulesArr.push((rules.age.type || "") + " age: " + (rules.age.value || 0));
    // }
    // if (rules.overweightCharge) {
    //   rulesArr.push("Overweight charge: " + rules.overweightCharge.charge + " " + rules.overweightCharge.currency);
    // }
    if (rules.cancellationCharge) {
      rulesArr.push("Cancellation charge: " + rules.cancellationCharge.value + " " + (rules.cancellationCharge.type == 'PERCENT'?'%':'$'));
    }

    // if (rules.daysOfWeek) {
    //   let text = "";
    //   text += rules.daysOfWeek.sunday ? "Sunday, " : "";
    //   text += rules.daysOfWeek.monday ? "Monday, " : "";
    //   text += rules.daysOfWeek.tuesday ? "Tuesday, " : "";
    //   text += rules.daysOfWeek.wednesday ? "Wednesday, " : "";
    //   text += rules.daysOfWeek.thursday ? "Thursday, " : "";
    //   text += rules.daysOfWeek.friday ? "Friday, " : "";
    //   text += rules.daysOfWeek.saturday ? "Saturday" : "";

    //   text = text.trim();
    //   text.length ? rulesArr.push("Operational On: " + text) : "";
    // }

    return rulesArr;
  }
  // onValueChange($event){
  //   this.select_today=false;
  //  this.select_tomorrow=false;
  // }
  onValueChange(event) {
    // this.dateVal = event;
    if (event != null) {
      this.select_today = false;
      this.select_tomorrow = false;
      this.select_specific_date = true;
    }
    
  }
  // onChange(event){
  //   this.dateVal=event;
  //   this.select_today = false;
  //    this.select_tomorrow = false;
  //    console.log(event);
  // }

  showNotification() {
    this.isShowAlert = true;
    setTimeout(() => {
      this.isShowAlert = false;
    }, 3000);

  }

}
