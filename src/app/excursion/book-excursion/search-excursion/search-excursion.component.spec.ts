import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchExcursionComponent } from './search-excursion.component';

describe('SearchExcursionComponent', () => {
  let component: SearchExcursionComponent;
  let fixture: ComponentFixture<SearchExcursionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchExcursionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchExcursionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
