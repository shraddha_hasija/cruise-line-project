import { Component, OnInit, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { SharedDataService } from 'src/app/shared/services/shared-data.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { HTTPService } from 'src/app/shared/services/http.service';
import { URLService } from 'src/app/shared/services/url.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.css']
})
export class BookingDetailsComponent implements OnInit {
  unique_array:any=[];
  travellerRecrod:any=[];
  bookingDetails: any = {};
  bsModalRef: BsModalRef;
  travellerList = [];
  adultList = [];
  childList = [];
  totalAdultCount: any = 0;
  totalChildCount: any = 0;
  adultTicketPrice: any = 0;
  childTicketPrice: any = 0;
  totalAdultAmount: any = 0;
  totalChildAmount: any = 0;
  totalAmountToPay: any = 0;
  finalAmountToPay: any = 0;
  addDecimal = 0.00;

  isCashMode = true;
  bookedTravellerList: any = [];
  bookExcursionDetails: any = {};
  // addBookingDetails={
  inclusionsContent = [];
  exclusionsContent = [];
  instructionsContent = [];
  description: any = "";
  excursionImageUrl = "";
  discountType = '';
  discountValue: any = 0;
  discountTitleToDisplay = '';
  revDiscountTitleToDisplay = '';
  //
  revTotalAdultCount: any = 0;
  revTotalChildCount: any = 0;
  revTotalAdultAmount: any = 0;
  revTotalChildAmount: any = 0;
  revTotalAmountToPay: any = 0;
  revFinalAmountToPay: any = 0;
  //

  //record change message
  changeAmountMsg: any = 0;
  changeCabinNumMsg:any = [];
  changeMessage:any = [];
  isRecordDeleted = false;
  // };
  // bookDetailsListarr:any=[];
  alert = {
    message: '',
    type: "",
    title: ""
  };
  isShowAlert = false;
  deleteTravellerIndex = 0;
  bsTravellerDelete: BsModalRef;
  constructor(private sharedService: SharedDataService,
    private httpService: HTTPService, private urlService: URLService, private _location: Location, private bsModalService: BsModalService, private router: Router) { }

  ngOnInit() {
    this.getBookingDetails();

    this.getBookExcursionDetails(this.bookingDetails["excrusionId"]);
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes);
  }

  getBookingDetails() {
    this.bookingDetails = this.sharedService.getBookingDetails();
    this.travellerList = this.bookingDetails["bookingUserPrices"];
    console.log(this.bookingDetails);

    //k
    if (this.travellerList && this.travellerList.length > 0) {
      for (let index = 0; index < this.travellerList.length; index++) {
        if (this.travellerList[index].traveller.isChild) {
          this.childList.push(this.travellerList[index].traveller.firstName + " " + this.travellerList[index].traveller.lastName);
          if (this.childTicketPrice === 0) {
            this.childTicketPrice = this.travellerList[index].price.toFixed(2);
          }
        }
        else {
          this.adultList.push(this.travellerList[index].traveller.firstName + " " + this.travellerList[index].traveller.lastName);
          if (this.adultTicketPrice === 0) {
            this.adultTicketPrice = this.travellerList[index].price.toFixed(2);
          }
        }
      }//for ends here
    }
    //
    // if (this.childTicketPrice === 0) {
    //   this.childTicketPrice = this.adultTicketPrice / 2;
    // }
    if (this.adultList.length > 0) {
      this.totalAdultCount = this.adultList.length
    }
    if (this.childList.length > 0) {
      this.totalChildCount = this.childList.length
    }
    this.totalAdultAmount = this.totalAdultCount.toFixed(2) * this.adultTicketPrice;
    this.totalChildAmount = this.totalChildCount.toFixed(2) * this.childTicketPrice;

    this.totalAmountToPay = this.totalAdultAmount + this.totalChildAmount;
    this.finalAmountToPay = this.totalAmountToPay;
    //
    if (this.bookingDetails["paymentMode"] === "CHARGETOCABIN") {
      this.isCashMode = false;
    } else {
      this.isCashMode = true;
    }
    //

    this.discountType = this.bookingDetails["discountType"];
    this.discountValue = this.bookingDetails["discountValue"];

    // PERCENT, FIXED, SALE
    if (this.discountType === 'PERCENT') {
      let discountedPrice: any = this.totalAmountToPay * this.discountValue / 100;
      discountedPrice = parseFloat(discountedPrice).toFixed(2);
      this.finalAmountToPay = this.totalAmountToPay.toFixed(2) - discountedPrice;
      this.discountTitleToDisplay = "(-Discount " + this.discountValue + '% ) - $' + discountedPrice;
    } else if (this.discountType === 'FIXED') {
      let tempDiscountPrice: any = this.discountValue;
      tempDiscountPrice = parseFloat(tempDiscountPrice).toFixed(2);
      this.finalAmountToPay = this.totalAmountToPay - this.discountValue;
      this.discountTitleToDisplay = "(-Discount " + this.discountValue + '$ ) - $' + tempDiscountPrice;
    } else if (this.discountType === 'SALE') {
      this.finalAmountToPay = this.totalAmountToPay;
      this.discountTitleToDisplay = "(-Discount " + 0 + '$) -$' + 0;
    }
    // /////////  revise variable initialize
    this.totalAdultAmount = parseFloat(this.totalAdultAmount).toFixed(2);
    this.totalChildAmount = parseFloat(this.totalChildAmount).toFixed(2);
    this.totalAmountToPay = parseFloat(this.totalAmountToPay).toFixed(2);
    this.finalAmountToPay = parseFloat(this.finalAmountToPay).toFixed(2);
    this.adultTicketPrice = parseFloat(this.adultTicketPrice).toFixed(2);
    this.childTicketPrice = parseFloat(this.childTicketPrice).toFixed(2);
  }

  setRevisePaymentData() {
    this.revTotalChildAmount = 0;
    this.revTotalAdultCount = 0;
    this.revTotalChildCount = 0;
    this.revTotalAdultAmount = 0;
    // bookedTravellerList
    for (let index = 0; index < this.bookedTravellerList.length; index++) {
      if (this.bookedTravellerList[index].traveller.isChild) {
        this.revTotalChildCount = this.revTotalChildCount + 1;
      } else {
        this.revTotalAdultCount = this.revTotalAdultCount + 1;
      }
    }

    this.revTotalAdultAmount = this.revTotalAdultCount * this.adultTicketPrice;
    this.revTotalChildAmount = this.revTotalChildCount * this.childTicketPrice;
    this.revTotalAmountToPay = this.revTotalAdultAmount + this.revTotalChildAmount;


    if (this.discountType === 'PERCENT') {
      let discountedPrice: any = this.revTotalAmountToPay * this.discountValue / 100;
      discountedPrice = parseFloat(discountedPrice).toFixed(2);
      this.revFinalAmountToPay = this.revTotalAmountToPay - discountedPrice;
      this.revDiscountTitleToDisplay = "(-Discount " + this.discountValue + '% ) - $' + discountedPrice;
    } else if (this.discountType === 'FIXED') {
      let tempDiscountPrice: any = this.discountValue;
      tempDiscountPrice = parseFloat(tempDiscountPrice).toFixed(2);
      this.revFinalAmountToPay = this.revTotalAmountToPay - this.discountValue;
      this.revDiscountTitleToDisplay = "(-Discount " + this.discountValue + '$ ) - $' + tempDiscountPrice;
    } else if (this.discountType === 'SALE') {
      this.revFinalAmountToPay = this.revTotalAmountToPay;
      this.revDiscountTitleToDisplay = "(-Discount " + 0 + '$) -$' + 0;
    }


    this.adultTicketPrice = parseFloat(this.adultTicketPrice).toFixed(2);
    this.childTicketPrice = parseFloat(this.childTicketPrice).toFixed(2);
    this.totalAdultAmount = parseFloat(this.totalAdultAmount).toFixed(2);
    this.totalChildAmount = parseFloat(this.totalChildAmount).toFixed(2);
    this.totalAmountToPay = parseFloat(this.totalAmountToPay).toFixed(2);
    this.finalAmountToPay = parseFloat(this.finalAmountToPay).toFixed(2);
    //
    this.revTotalAdultAmount = parseFloat(this.revTotalAdultAmount).toFixed(2);
    this.revTotalChildAmount = parseFloat(this.revTotalChildAmount).toFixed(2);
    this.revTotalAmountToPay = parseFloat(this.revTotalAmountToPay).toFixed(2);
    this.revFinalAmountToPay = parseFloat(this.revFinalAmountToPay).toFixed(2);

  }

  getBookExcursionDetails(excrusionId) {
    // getBookingById
    this.httpService.call("getExcusionDetailById", excrusionId).subscribe((data) => {
      this.bookExcursionDetails = data;
      console.log(data);
      this.setExcursionDetails(data);

    }, (error) => {
      this.alert = {
        type: "error",
        message: 'Some error occured',
        title: "Failed"
      };
      this.showNotification();
    });
  }

  setExcursionDetails(exclusionsDetails) {
    //
    if (exclusionsDetails.content.media && exclusionsDetails.content.media.length > 0
      && exclusionsDetails.content.media[0].url && exclusionsDetails.content.media[0].url.length > 1) {
      this.excursionImageUrl = exclusionsDetails.content.media[0].url;
    } else {
      this.excursionImageUrl = "../../assets/images/mapimg.png"
    }

    //
    let stringToSplit = "abc def ghi";
    let x = stringToSplit.split(" ");
    console.log(x[0]);

    if (exclusionsDetails.content.inclusions) {
      let strInclusions = exclusionsDetails.content.inclusions.replace(/<\/?[^>]+>/ig, " ");
      this.inclusionsContent = strInclusions.split(";");
      console.log(this.inclusionsContent)

    }
    if (exclusionsDetails.content.exclusions) {
      let strExclusion = exclusionsDetails.content.exclusions.replace(/<\/?[^>]+>/ig, " ");;
      this.exclusionsContent = strExclusion.split(";");


    }

    if (exclusionsDetails.content.instructions) {
      let strInstruction = exclusionsDetails.content.instructions.replace(/<\/?[^>]+>/ig, " ");;
      this.instructionsContent = strInstruction.split(";");

    }

    if (exclusionsDetails.content.description) {
      this.description = exclusionsDetails.content.description.replace(/<\/?[^>]+>/ig, " ");;
    }

  }

  showEditBookingDialog(template) {
    this.bookedTravellerList = this.bookingDetails ? this.bookingDetails["bookingUserPrices"].map(x => Object.assign({}, x)) : '';

    for (let i = 0; i < this.bookedTravellerList.length; i++) {
      this.bookedTravellerList[i]["isDisabled"] = true;
    }
    console.log(this.bookedTravellerList);
    console.log(this.bookingDetails);

    let isCancelled = this.bookingDetails['isCancelled'];
    this.setRevisePaymentData();
    if (1 === 1) {
      this.isRecordDeleted = false;
      this.changeAmountMsg = 0;
      this.changeCabinNumMsg = [];
      this.changeMessage = [];
      this.unique_array=[];
      this.bsModalRef = this.bsModalService.show(template, { class: 'modify-booking-modal modal-lg' });
    } else {
      this.alert = {
        type: "Alert",
        title: "Alert",
        message: "Booking already cancelled"
      };
      this.showNotification();
    }
  }

  deleteTravellerDialog(no, template) {

    if (this.bookedTravellerList.length > 1) {
      this.deleteTravellerIndex = no;
      this.bsTravellerDelete = this.bsModalService.show(template, { class: 'modify-booking-modal modal-md' });
    }
  }
  deleteSelectedTraveller() {
    this. travellerRecrod= this.bookedTravellerList[this.deleteTravellerIndex];
    this.isRecordDeleted = true
  
    let ticketPrice = !this.travellerRecrod["isChild"] ? this.adultTicketPrice : this.childTicketPrice;
    this.changeAmountMsg = +this.changeAmountMsg + (+ticketPrice);
    
    // this.changeCabinNumMsg = this.travellerRecrod["traveller"].cabin;
    for(let i=0;i<1;i++){
      this.changeCabinNumMsg.push(this.travellerRecrod["traveller"].cabin)
    }
    for(let i=0;i<=this.changeCabinNumMsg.length;i++){
      if(this.unique_array.indexOf(this.changeCabinNumMsg[i]) == -1){
        this.unique_array.push(this.changeCabinNumMsg[i])
    }
    }
    // this.changeMessage =
    //   this.changeMessage.concat(travellerRecrod["traveller"].firstName + " " + travellerRecrod["traveller"].lastName + " is removed from booking" , ',')
    for(let i=0;i<1;i++){
      this.changeMessage.push(this.travellerRecrod["traveller"].firstName + " " + this.travellerRecrod["traveller"].lastName + " is removed from booking")
  
    }
      
    this.bookedTravellerList.splice(this.deleteTravellerIndex, 1);
    console.log(this.changeMessage);
    console.log(this.bookedTravellerList);
    this.setRevisePaymentData();
    this.bsTravellerDelete.hide();
  }

  showNotification() {
    this.isShowAlert = true;
    console.log(this.isShowAlert)
    setTimeout(() => {
      this.isShowAlert = false;
    }, 1000);
  }

  backClicked() {
    this._location.back();
  }

  updateTravellerAndBooking() {
    console.log(this.bookedTravellerList.length);
    // let url = "updateTravellerList";
    // this.httpService.call(url, this.bookedTravellerList).subscribe((data) => {
    //   console.log("traveller list updated successfully")
    //   this.updateBooking();
    // })
    this.updateBooking();
  }

  updateBooking() {
    console.log(this.bookedTravellerList.length);
    let url = "updateBookedExcursion";
    this.bookingDetails["noOfTickets"] = this.bookedTravellerList.length;

    this.bookingDetails['bookingUserPrices'] = this.bookedTravellerList;
    this.bookingDetails['totalAmount'] = this.revFinalAmountToPay;
    console.log(this.bookingDetails);
    console.log(this.bookedTravellerList);

    this.httpService.call(url, this.bookingDetails).subscribe((data) => {

      this.alert = {
        type: "success",
        title: "Success",
        message: "Booking updated successfully"
      };
      console.log("upDated Successfully");
      this.bsModalRef.hide();
      this.showNotification()
      this.router.navigate(['/excursion/book/list']);
    }, (error) => {
      console.log("something went wrong");
      this.alert = {
        type: "error",
        title: "Failed",
        message: "Some error occured"
      };
      this.showNotification();
    });
  }

  cancelSelectedBooking() {
    let bookingId = this.bookingDetails["id"];
    console.log("booking id is" + bookingId);
    this.bsModalRef.hide();
    this.httpService.call("cancelBooking", null, bookingId).subscribe((data) => {
      this.alert = {
        type: "success",
        title: "Success",
        message: "Booking canceled successfully"
      };
      console.log("deleted Successfully");
      this.showNotification()
      this.router.navigate(['/excursion/book/list']);
    }, (error) => {
      console.log("something went wrong");
      this.alert = {
        type: "error",
        title: "Failed",
        message: "Some error occured"
      };
      this.showNotification();
    });
  }
  editTraveller(no) {
    this.bookedTravellerList[no]["isDisabled"] = !this.bookedTravellerList[no]["isDisabled"];
  }

  editHeightValue(event, no) {
    this.bookedTravellerList[no]["traveller"]["height"] = event.target.value;
    console.log(this.bookedTravellerList);
  }

  editWeightValue(event, no) {
    this.bookedTravellerList[no]["traveller"]["weight"] = event.target.value;
  }

  showConfirmCancelDialog(template) {
    let isCancelled = this.bookingDetails['isCancelled'];
    if (!isCancelled) {
      this.bsModalRef = this.bsModalService.show(template, Object.assign({}, { class: 'modal-md' }));
    } else {
      this.alert = {
        type: "Alert",
        title: "Alert",
        message: "Booking already cancelled"
      };
      this.showNotification();
    }
  }
}
