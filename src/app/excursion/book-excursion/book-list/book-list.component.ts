import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { CommonUtilityService } from 'src/app/shared/services/common-utility.service';
import { HTTPService } from 'src/app/shared/services/http.service';
import { URLService } from 'src/app/shared/services/url.service';
import { Router } from '@angular/router';
import { SharedDataService } from 'src/app/shared/services/shared-data.service';
import { DatePipe } from '@angular/common';
import { map } from 'rxjs/operators';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css'],
  providers: [DatePipe]
})
export class BookListComponent implements OnInit {
  // @Output() notify: EventEmitter<any> = new EventEmitter();
  currPage=1;
  noOfItems=10;
  isShowAlert: boolean = false;
  alert = {
    message: '',
    type: "",
    title: ""
  };
  bookingTypeList = ["Cancel Booking", "Print"];
  selectedType = this.bookingTypeList[0];
  selectAllRows = false;
  bookingList: any = [];
  cabinNumber = 0;
  modalRef: BsModalRef;
  selectedBookingItemId = 0;

  constructor(private commonService: CommonUtilityService, private httpService: HTTPService,
    private urlService: URLService, private router: Router,
    private modalService: BsModalService, private sharedDataService: SharedDataService) { }


  ngOnInit() {
    this.getBookingList();
  }

  getBookingList() {

    this.httpService.call("getBookingList").subscribe((data) => {
      // this.cabinNumber = data[3].bookingUserPrices[0].traveller.cabin;
      this.bookingList = data;
      // let bookingDetail=this.bookingList.map((data)=>this.datePipe.transform(data.travelDate,'YYYY-MM-DD'))
      // this.bookingList.travelDate=this.datePipe.transform(this.bookingList.travelDate,'yyyy-dd-MM');
      // console.log(bookingDetail);
    }, (error) => {
      // this.alert = {
      //   type: "error",
      //   message: 'Some error occured',
      //   title: "Failed"
      // };
      // this.notify.emit(this.alert);
    });


  }


  onBokingTypeChange() {

  }

  deleteSelectedBooking() {

    // console.log(url);
    // this.alert = {
    //   type: "success",
    //   title: "Success",
    //   message: "Booking deleted successfully"
    // };
    // this.showNotification();
    // this.notify.emit(this.alert);
    this.modalRef.hide();
    console.log("booking id is" + this.selectedBookingItemId);
    this.httpService.call("cancelBooking", null, this.selectedBookingItemId).subscribe((data) => {
      this.alert = {
        type: "success",
        title: "Success",
        message: "Booking deleted successfully"
      };
      this.refresh();
      this.showNotification();
    }, (error) => {
      this.alert = {
        type: "error",
        title: "Failed",
        message: "Some error occured"
      };
      this.showNotification();
    })
  }


  showNotification() {
    this.isShowAlert = true;
    setTimeout(() => {
      this.isShowAlert = false;
    }, 1000);
    // this.alert = event;
  }
  showConfirmDeleteDialog(id, template) {
    //here check id from list, and then check if already deleted show alert notification
    this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-md' }));
    this.selectedBookingItemId = id;
  }


  refresh() {
    this.getBookingList();
  }
  printBooking(id) {

  }

  editBooking(row) {
    if(!row.isCancelled){
    this.sharedDataService.setBookingDetails(row);
    this.router.navigate(['/excursion/book/details']);
    }
  }

  next() {

  }

  changeAllCheckboxSelection(event) {

  }

}
