import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { HTTPService } from 'src/app/shared/services/http.service';



@Component({
  selector: 'app-booking-details-modal',
  templateUrl: './booking-details-modal.component.html',
  styleUrls: ['./booking-details-modal.component.css']
})
export class BookingDetailsModalComponent implements OnInit {
  isShowAlert: boolean = false;
  isDiscount: boolean = true;
  isDiscountAmount: boolean = true;
  isSalePrice: boolean = true;
  btnCaseSelected: boolean = false;
  btnCabinSelected: boolean = false;
  title: string;
  closeBtnName: string;
  date:any;
  list: any[] = [];
  excursionData: any = {};
  travellerData = [];
  payMode = "";
  optionCent = false;
  optionDiscount = false;
  optionSale = false;
  discountCent: any = "";
  discountVal: any = "";
  salePrice = "";
  comments = "";
  options = "";
  noOfTickets = 0;
  totalAmt: number = 0;
  adultPrice = 0;
  childPrice = 0;
  alert = {
    message: '',
    type: "",
    title: ""
  };

  amounttoPayAfterDiscount: any = 0;
  discountedPrice: any = 0;

  constructor(public bsModalRef: BsModalRef, private router: Router,
    private httpService: HTTPService) { }

  ngOnInit() {
    this.excursionData = this.list[0] ? this.list[0] : {};
    this.travellerData = this.excursionData ? this.excursionData.users : [];
    this.noOfTickets = this.travellerData.length;

    this.selectBookOptions("cent");
    // this.selectPayModeCash("CASH");
    this.getTotalAmount();
  }

  calculateDiscount(value) {
    console.log("value is------" + value);
    value = +value;
    if (this.options === "cent") {
      let discountPrice = (this.totalAmt * value) / 100;
      this.amounttoPayAfterDiscount = this.totalAmt - discountPrice;
      this.discountedPrice = discountPrice;
    }
    else if (this.options === "discountVal") {
      this.discountVal = +this.discountVal;
      this.amounttoPayAfterDiscount = this.totalAmt - value;
      this.discountedPrice = this.discountVal;

    } else if (this.options === "sale") {
      this.amounttoPayAfterDiscount = this.totalAmt;
      this.discountedPrice = 0;
    }

  }

  selectBookOptions(options) {
    this.options = options;
  }
  selectPayModeCash(payMode) {
    this.payMode = payMode;
    this.btnCaseSelected = true;
    this.btnCabinSelected = false;
  }
  selectPayModeCabin(payMode) {
    this.payMode = payMode;
    this.btnCaseSelected = false;
    this.btnCabinSelected = true;
  }

  bookExcursion() {
    let data = {};
    data["excursionName"] = this.excursionData ? this.excursionData.name || null : null;
    data["travelDate"] = this.date;
    data["status"] = "Confirmed";
    // 
    data["parentTravellerName"] = this.travellerData[0].firstName + " " + this.travellerData[0].lastName;

    data["noOfTickets"] = this.noOfTickets || 0;
    data["comments"] = this.comments || "";
    data["paymentMode"] = this.payMode || "";

    if (this.options === "cent") {
      data["discountType"] = "PERCENT";
      data["discountValue"] = this.discountCent || 0;
      let discountPrice = (this.totalAmt * this.discountCent) / 100;
      this.amounttoPayAfterDiscount = this.totalAmt - discountPrice;
      this.discountedPrice = discountPrice;
    }
    else if (this.options === "discountVal") {
      data["discountType"] = "FIXED";
      data["discountValue"] = this.discountVal || 0;
      this.amounttoPayAfterDiscount = this.totalAmt - this.discountVal;
      this.discountedPrice = this.discountVal;

    } else if (this.options === "sale") {
      data["discountType"] = "SALE";
      data["discountValue"] = this.salePrice || 0;
      this.amounttoPayAfterDiscount = this.totalAmt;
      this.discountedPrice = 0;;
    }

    data["excrusionId"] = this.excursionData ? this.excursionData.id : null;
    data["bookingMode"] = "POS";
    data["bookingUserPrices"] = this.getTravellerListWithPrice();

    data["totalAmount"] = this.amounttoPayAfterDiscount;

    //
    console.log(JSON.stringify(data));
    this.callSaveExcursion(data);
  }

  getTravellerListWithPrice() {
    let lstOfTravellers = [];
    for (let index = 0; index < this.travellerData.length; index++) {
      let arrObj = {};
      let travellerObj = {};
      travellerObj["id"] = this.travellerData[index] ? this.travellerData[index].id : null;
      travellerObj["firstName"] = this.travellerData[index] ? this.travellerData[index].firstName : null;
      travellerObj["lastName"] = this.travellerData[index] ? this.travellerData[index].lastName : null;
      travellerObj["email"] = this.travellerData[index] ? this.travellerData[index].email : null;
      travellerObj["phoneNumber"] = this.travellerData[index] ? this.travellerData[index].phoneNumber : null;

      travellerObj["age"] = this.travellerData[index] ? this.travellerData[index].age : null;
      travellerObj["gender"] = this.travellerData[index] ? this.travellerData[index].gender : null;
      travellerObj["weight"] = this.travellerData[index] ? this.travellerData[index].weight : null;
      travellerObj["height"] = this.travellerData[index] ? this.travellerData[index].height : null;
      travellerObj["status"] = this.travellerData[index] ? this.travellerData[index].status : null;

      travellerObj["cabin"] = this.travellerData[index] ? this.travellerData[index].cabin : null;
      travellerObj["isChild"] = this.travellerData[index] ? this.travellerData[index].isChild : null;
      travellerObj["parentTravellerId"] = this.travellerData[index] ? this.travellerData[index].parentTravellerId : null;
      travellerObj["dependents"] = [];
      ///
      arrObj["traveller"] = travellerObj;
      // arrObj["price"] = this.excursionData ? this.excursionData.prices[0].adult : null;
      if (travellerObj["isChild"]) {
        arrObj["price"] = this.excursionData ? this.excursionData.prices[0].child : 0;
      } else {
        arrObj["price"] = this.excursionData ? this.excursionData.prices[0].adult : 0;
      }

      lstOfTravellers.push(arrObj);
    }
    return lstOfTravellers;
  }

  callSaveExcursion(bookingData) {
    let url = "saveBooking";

    this.httpService.call(url, bookingData).subscribe((data) => {

      let title = "Success";
      let message = "Booking is done successfully"
      this.alert = {
        type: "success",
        title: title,
        message: message
      };
      this.showNotification();

      this.bsModalRef.hide();
      // this.alert = {
      //   type: "error",
      //   message: 'No Records Found',
      //   title: ""
      // };
      // this.showNotification();
      this.router.navigate(['/excursion/book/list']);



      console.log("excusion book successfully");
    }, (error) => {
      this.bsModalRef.hide();
      console.log("excusion book error occur");
      // this.alert = {
      //   type: "error",
      //   title: "Failed",
      //   message: "Something went wrong"
      // };
      // this.showNotification();
      // this.notify.emit(this.alert);
    });
  }
  showNotification() {
    this.isShowAlert = true;
    console.log(this.isShowAlert)
    setTimeout(() => {
      this.isShowAlert = false;
    }, 3000);
  }


  getTotalAmount() {
    this.adultPrice = this.excursionData.prices[0].adult;
    this.childPrice = this.excursionData.prices[0].child;
    for (let index = 0; index < this.travellerData.length; index++) {

      if (this.travellerData[index].isChild) {
        this.totalAmt = this.totalAmt + parseFloat(this.excursionData.prices[0].child);
      } else {
        this.totalAmt = this.totalAmt + parseFloat(this.excursionData.prices[0].adult);
      }
    }
    this.amounttoPayAfterDiscount = this.totalAmt;
  }

  isDicountAmountDisable() {
    this.isDiscountAmount = false;
    this.isDiscount = true;
    this.isSalePrice = true;
    this.discountCent = '';
    this.salePrice = '';
    this.amounttoPayAfterDiscount = this.totalAmt;
    this.discountedPrice = 0;

  }
  isSalePriceDisable() {
    this.isSalePrice = false;
    this.isDiscount = true;
    this.isDiscountAmount = true;
    this.discountCent = '';
    this.discountVal = '';
    this.amounttoPayAfterDiscount = this.totalAmt;
    this.discountedPrice = 0;

  }
  isDiscountDisable() {
    this.isSalePrice = true;
    this.isDiscountAmount = true;
    this.isDiscount = false;
    this.salePrice = '';
    this.discountVal = '';
    this.amounttoPayAfterDiscount = this.totalAmt;
    this.discountedPrice = 0;

    // this.discountAmount.nativeElement.value="";
    // this.salePriceVal.nativeElement.value="";

  }
}
