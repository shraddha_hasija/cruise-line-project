import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { HTTPService } from 'src/app/shared/services/http.service';
import { URLService } from 'src/app/shared/services/url.service';
import { IfStmt } from '@angular/compiler';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-new-excursion',
  templateUrl: './new-excursion.component.html',
  styleUrls: ['./new-excursion.component.css']
})
export class NewExcursionComponent implements OnInit {

  @Input() editData = {};
  @Input() listTabActive = false;
  @Output() resetTab: EventEmitter<any> = new EventEmitter();
  @Output() notify: EventEmitter<any> = new EventEmitter();

  readonly typeEnum = {
    Min: "MIN",
    Max: "MAX"
  };
  readonly cruiseTypeEnum = {
    "Mid-Cruise": "MIDCRUISE",
    "Pre-Cruise": 'PRECRUISE',
    "Post-Cruise": "POSTCRUISE"
  };
  readonly typeValues = {
    "MIN": "Min",
    "MAX": "Max"
  };
  readonly cruiseTypeValues = {
    "MIDCRUISE": "Mid-Cruise",
    "PRECRUISE": "Pre-Cruise",
    "POSTCRUISE": "Post-Cruise"
  };
  DEFAULT_VENDOR = ""
  DEFAULT_COUNTRY = "";
  DEFAULT_PORT = "";
  DEFAULT_CATEGORY = "";
  DEFAULT_ACTIVITY_LEVEL = "Easy";
  DEFAULT_CRUISE_TYPE = "";
  DEFAULT_CURRENCY = "USD";

  alert = {
    message: '',
    type: "",
    title: ""
  };
  // status={
  //   isMakeActive:false,
  //   isMakeInactive:false
  // }
  isMakeActive:boolean=false;
  isMakeInactive:boolean=false;
  isStep1Active: boolean = true;
  isStep2Active: boolean = false;
  isEdit: boolean = false;
  description = "";
  instructions = "";
  inclusions = "";
  exclusions = "";
  editorConfig = {
    minHeight: 100,
    width: 300,
    placeholder: "Enter text here..",
    spellcheck: true
  };
  dpConfig = {
    dateInputFormat: 'MMM DD YYYY',
    containerClass: 'theme-red'
  }
  startTime = "";
  meetingTime = "";
  selectedVendor = '';
  selectedCountry = this.DEFAULT_COUNTRY;
  selectedPort = this.DEFAULT_PORT;
  categoryList = ["Adventure", "Club Mariner", "Concierge Choice", "Diving", "Hiking", "Refreshments", "Seated", "Special Event", "Walking", "Water", "Wheelchair"];
  selectedCategory = this.DEFAULT_CATEGORY;
  vendorList = ['Bindlestiff-tours', 'Trek-america', 'G-adventures', 'Contiki','Insight-vacations','Trafalgar','Topdeck','Intrepid','Cosmos','Grand-american-adventures'];
  countryList = ["USA", "Canada", "Russia", "Ukraine", "France", "Spain", "Sweden", "Norway", "Germany", "Finland", "Poland", "Italy", "United Kingdom", "Romania", "Belarus", "Kazakhstan", "Greece", "Bulgaria", "Iceland", "Hungary", "Portugal", "Austria", "Czech Republic", "Serbia", "Ireland", "Lithuania", "Latvia", "Croatia", "Bosnia and Herzegovina", "Slovakia", "Estonia", "Denmark", "Switzerland", "Netherlands", "Moldova", "Belgium", "Albania", "Macedonia", "Turkey", "Slovenia", "Montenegro", "Kosovo", "Cyprus", "Azerbaijan", "Luxembourg", "Georgia", "Andorra", "Malta", "Liechtenstein", "San Marino", "Monaco", "Vatican City"];
  portList = ['Port Everglades', 'Port of Houston', 'Port of New York and New Jersey', 'Port Newark','Port of Beaumont','Port of Long Beach','Port of Hampton Roads','Port of New Orleans'];
  languageList = ['English', 'German', 'Spanish', 'French'];
  currencyList = ["USD"];
  // activityLevel:any = ["Easy", "Moderate", "Difficult"];
  activityLevel:any = ["Easy", "Moderate", "Difficult"];
  typeList = ["Mid-Cruise", "Pre-Cruise", "Post-Cruise"];
  selectedType = this.DEFAULT_CRUISE_TYPE;
  selectedActivity:any = this.DEFAULT_ACTIVITY_LEVEL
  selectedCurrency = this.DEFAULT_CURRENCY;
  excursionName = "";
  startDate =new Date();
  endDate = new Date();
  meetingPlace = "";
  durationMins: number = 0;
  durationHrs: number = 0;
  minCapacity: number = 0;
  maxCapacity: number = 0;
  maxAdult = "";
  maxChild = "";
  price = "";
  pckgPriceVal = "";
  priceRows = [{
    priceValidFrom: new Date(),
    priceValidTo: new Date(),
    adult: "",
    child: "",
    no: +new Date() // unique index
  }];
  isWeightRule = false;
  weightVal = "";
  weightTypes = ["Min", "Max"];
  weightTypeSelected = "Max";
  heightTypes = ["Min", "Max"];
  heightTypeSelected = "Max";
  ageTypes = ["Min", "Max"];
  ageTypeSelected = "Max";
  isAgeRule = false;
  ageVal = "";
  isHeightRule = false;
  heightVal = "";
  isOverWeightRule = false;
  overweightVal = "";
  isCancellationRule = false;
  cancellationCharge = "";
  cancellationType = "Fixed";
  overWeightChargesType='Fixed';
  isNoPromotionRule: boolean = false;
  isOneTicketPerPersonRule: boolean = false;
  embarkingGuest: boolean = false;
  disembarkingGuest: boolean = false;
  intransitGuest: boolean = false;
  isPublishOnApp: boolean = false;
  isPublishOnWeb: boolean = false;
  isPublishOnKisok: boolean = false;
  isPublishOnChatbot: boolean = false;
  daysOfWeek = {
    "sunday": false,
    "monday": false,
    "tuesday": false,
    "wednesday": false,
    "thursday": false,
    "friday": false,
    "saturday": false
  };
  status :any = "ACTIVE";
  language = "English";
  // mediaList = [];
  transType = "";
  transCom = "";
  id = null;
  isInsuranceReq: boolean = false;
  isAC: boolean = false;
  isCovered: boolean = false;
  formSubmitted: boolean = false;
  counter = 1;
  mediaFileList = [{ url: "", index: this.counter }];
  constructor(private httpService: HTTPService, private urlService: URLService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if (changes.editData && !changes.editData.isFirstChange()) {
      this.isEdit = true;
      this.setExcursionData(changes.editData.currentValue);
    }

    if (changes.listTabActive && changes.listTabActive.currentValue && !changes.listTabActive.isFirstChange()) {
      this.isStep1Active = true;
      this.isStep2Active = false;
    }
  }

  addMediaFile() {
    this.counter = this.counter + 1;
    let newMediaFile = {
      url: "",
      index: this.counter
    };
    this.mediaFileList.push(newMediaFile);
    console.log(this.mediaFileList);
  }

  removeMediaFile(i) {
    if (this.mediaFileList.length > 1) {
      this.mediaFileList.splice(i, 1);
      console.log(this.priceRows);
    }
  }

  setExcursionTime(timeStr) {
    let dateObj = new Date();

    if (timeStr && timeStr.indexOf(":") > -1) {
      let timeArr = timeStr.split(":");
      let hours = timeArr[0] || 0;
      let min = timeArr[1] || 0;

      dateObj.setHours(hours);
      dateObj.setMinutes(min);
    }
    return +dateObj;
  }

  setDefaultValues() {
    this.selectedVendor = this.DEFAULT_VENDOR;
    this.selectedCountry = this.DEFAULT_COUNTRY;
    this.selectedPort = this.DEFAULT_PORT;
    this.selectedType = this.cruiseTypeValues["MIDCRUISE"];
    this.selectedActivity = this.DEFAULT_ACTIVITY_LEVEL;
    this.selectedCurrency = this.DEFAULT_CURRENCY;
    this.selectedCategory = this.DEFAULT_CATEGORY;

    this.startDate = new Date();
    this.endDate = new Date();
  }

  resetForm() {
    this.setExcursionData({});
    this.setDefaultValues();
  }

  addNew() {
    this.resetForm();
    this.isEdit = false;
    this.resetTab.emit();
  }

  setExcursionData(data) {
    // console.log(data)
    this.id = data.id ? data.id : null;
    this.excursionName = data.name ? data.name : "";
    this.selectedVendor = data.vendor ? data.vendor : "";
    this.selectedCountry = data.country ? data.country : "";
    this.selectedPort = data.port ? data.port : '';

    this.startTime = moment(this.setExcursionTime(data.startTime || "")).format("HH:mm");
    this.meetingTime = moment(this.setExcursionTime(data.meetingTime || "")).format("HH:mm");

    this.meetingPlace = data.meetingPlace ? data.meetingPlace : '';
    this.durationMins = data.duration ? data.duration % 60 : 0;
    this.durationHrs = data.duration ? parseInt((data.duration / 60) + "") : 0;
    this.selectedCategory = data.category ? data.category : "";
    // this.selectedActivity = data.activityLevel ? data.activityLevel : "";

    // ["1-Easy", "2-Moderate", "3-Difficult"
    if(data.activityLevel == '1'){
      this.selectedActivity='Easy';
    }
    if(data.activityLevel == '2'){
      this.selectedActivity='Moderate';
    }
    if(data.activityLevel == '3'){
      this.selectedActivity='Difficult';
    }
    this.selectedType = data.cruiseType ? this.cruiseTypeValues[data.cruiseType] : "";

    this.minCapacity = (data.capacity && data.capacity.min) ? data.capacity.min : 0;
    this.maxCapacity = (data.capacity && data.capacity.max) ? data.capacity.max : 0;
    this.startDate = (data.activeDuration && data.activeDuration.startDate) ? new Date(data.activeDuration.startDate) : null;
    this.endDate = (data.activeDuration && data.activeDuration.endDate) ? new Date(data.activeDuration.endDate) : null;

    this.transType = data.transportation ? (data.transportation.type || "") : "";
    this.transCom = data.transportation ? (data.transportation.comment || "") : "";
    this.isAC = data.transportation ? (data.transportation.isACAvailable || "") : "";
    this.isInsuranceReq = data.transportation ? (data.transportation.isInsuranceRequired || "") : "";
    this.isCovered = data.transportation ? (data.transportation.isCovered || "") : "";

    this.price = data.price ? data.price : [];

    this.weightVal = data.rules ? data.rules.weight ? data.rules.weight.value : "" : "";
    this.weightTypeSelected = data.rules ? data.rules.weight ? this.typeValues[data.rules.weight.type] : "" : "";
    this.isWeightRule = data.rules ? data.rules.weight ? true : false : false;

    this.heightVal = data.rules ? data.rules.height ? data.rules.height.value : "" : "";
    this.heightTypeSelected = data.rules ? data.rules.height ? this.typeValues[data.rules.height.type] : "" : "";
    this.isHeightRule = data.rules ? data.rules.height ? true : false : false;

    this.ageVal = data.rules ? data.rules.age ? data.rules.age.value : "" : "";
    this.ageTypeSelected = data.rules ? data.rules.age ? this.typeValues[data.rules.age.type] : "" : "";
    this.isAgeRule = data.rules ? data.rules.age ? true : false : false;

    this.cancellationCharge = data.rules ? data.rules.cancellationCharge ? data.rules.cancellationCharge.value : "" : "";
    this.cancellationType = data.rules ? data.rules.cancellationCharge ? (data.rules.cancellationCharge.type === "PERCENT" ? "%" : "Fixed") : "" : "";
    this.isCancellationRule = data.rules ? data.rules.cancellationCharge ? true : false : false;

    this.overweightVal = data.rules ? data.rules.overweightCharge ? data.rules.overweightCharge.charge : "" : "";
    this. overWeightChargesType =data.rules ? data.rules.overweightCharge ? (data.rules.overweightCharge.type === "PERCENT" ? "%" : "Fixed"): "" : "";
    // this.selectedCurrency = data.rules ? data.rules.overweightCharge ? data.rules.overweightCharge.currency : "" : "";
    this.isOverWeightRule = data.rules ? data.rules.overweightCharge ? true : false : false;

    this.daysOfWeek = data.rules ? data.rules.daysOfWeek || {} : {};

    // this.status = data.rules?data.rules.status ? true: false: false;
    // this.status=data.rules? data.rules.status || {}: {};
    this.isNoPromotionRule = data.rules ? data.rules.applyPromotion ? data.rules.applyPromotion : false : false;
    this.isOneTicketPerPersonRule = data.rules ? data.rules.oneTicketPerPerson || false : false;
    this.embarkingGuest = data.rules ? data.rules.allowEmbarkingGuests || false : data.rules;
    this.disembarkingGuest = data.rules ? data.rules.allowDisembarkingGuests || false : false;
    this.intransitGuest = data.rules ? data.rules.allowInTransitGuests || false : false;

    this.isPublishOnApp = data.publishOn ? data.publishOn.app || false : false;
    this.isPublishOnWeb = data.publishOn ? data.publishOn.web || false : false;
    this.isPublishOnKisok = data.publishOn ? data.publishOn.kisok || false : false;
    this.isPublishOnChatbot = data.publishOn ? data.publishOn.chat || false : false;

    this.language = data.content ? data.content.language || "English" : "English";
    this.inclusions = data.content ? data.content.inclusions || "" : "";
    this.exclusions = data.content ? data.content.exclusions || "" : "";
    this.description = data.content ? data.content.description || "" : "";
    this.instructions = data.content ? data.content.instructions || "" : '';

    this.priceRows = this.setPriceRows(data.prices || []);
    this.mediaFileList = data.content ? data.content.media ? this.setMediaUrl(data.content.media) : [] : [];
  }

  setMediaUrl(arr) {
    let newArr = [];

    for (let i = 0; i < arr.length; i++) {
      let obj = Object.assign({}, JSON.parse(JSON.stringify(arr[i])));

      obj["index"] = this.counter;
      this.counter++;
      newArr.push(obj);
    }
    return newArr;
  }

  setPriceRows(arr) {
    let newArr = [];

    for (let i = 0; i < arr.length; i++) {
      let obj = Object.assign({}, JSON.parse(JSON.stringify(arr[i])));
      obj.priceValidFrom = new Date(obj.validFrom);
      obj.priceValidTo = new Date(obj.validTo);
      obj["no"] = i;
      newArr.push(obj);
    }
    return newArr;
  }

  resetExcursionData() { }

  addPriceRows() {
    let newPrice = {
      priceValidFrom: new Date(),
      priceValidTo: new Date(),
      adult: "",
      child: "",
      no: +new Date()
    };
    //this.priceRows.push(newPrice);

    this.priceRows[this.priceRows.length] = newPrice;
    console.log(this.priceRows);
  }

  removePriceRow(i) {
    if (this.priceRows.length > 1) {
      this.priceRows.splice(i, 1);
      console.log(this.priceRows);
    }
  }

  onVendorChange(vendor) {
    this.selectedVendor = vendor;
  }

  onCountryChange(country) {
    this.selectedCountry = country;
  }

  onPortChange(port) {
    this.selectedPort = port;
  }

  onLangChange(language) {
    this.language = language;
  }

  onCategoryChange(cat) {
    this.selectedCategory = cat;
  }

  onActivityChange(activity) {
    // if(activity=='1-Easy')
    // {
    //   this.selectedActivity ='1';
    // }
    // if(activity=='2-Moderate')
    // {
    //   this.selectedActivity ='2';
    // }
    // else{
    // this.selectedActivity = '3';
    // }
    this.selectedActivity=activity
  }

  onTypeChange(type) {
    this.selectedType = type;
  }
  onOverWeightTypeCharge(type){
    this.overWeightChargesType=type;
  }

  onWeightChange(weight) {
    this.weightTypeSelected = weight;
  }

  onHeightChange(height) {
    this.heightTypeSelected = height;
  }

  onAgeChange(val) {
    this.ageTypeSelected = val;
  }

  onCurrChange(currency) {
    this.selectedCurrency = currency;
  }

  onCancellatiionTypeChange(type) {
    this.cancellationType = type;
  }

  updateTimeFormat(value) {
    if (value == "meeting") {
      this.meetingTime = moment(this.meetingTime).format("HH:mm");
    } else {
      this.startTime = moment(this.startTime).format("HH:mm");
    }
    console.log(this.startTime);
  }

  // onFileSelection(event) {
  //   if (event.target.files && event.target.files[0]) {
  //     var reader = new FileReader();

  //     reader.readAsDataURL(event.target.files[0]); // read file as data url

  //     reader.onloadend = (event: any) => { // called once readAsDataURL is completed
  //       this.mediaList.push(event.target.result);
  //     }
  //     console.log(this.mediaList)
  //   }

  // }

  validateStartTime() {
    if (!this.startTime || (typeof (this.startTime) != 'object' && !this.startTime.trim().length)) {
      return false;
    } else {
      return true;
    }
  }

  validateMeetingTime() {
    if (!this.meetingTime || !this.meetingTime.length) {
      return false;
    }
    return true;
  }

  validateDuration() {
    if ((!this.durationHrs && !this.durationMins) || isNaN(this.durationHrs) || isNaN(this.durationMins)) {
      return false;
    }
    return true;
  }

  validateDate() {
    //!(this.startDate instanceof Date && !isNaN(this.startDate))
    let isValid = false;

    if (!this.startDate) {
      isValid = false;
    }
    else if (!this.endDate) {
      isValid = false;
    }
    else if (+this.endDate < +this.startDate) {
      isValid = false;
    }
    else {
      isValid = true;
    }
    return isValid;
  }

  validateName() {
    if (!this.excursionName || !this.excursionName.trim().length) {
      return false;
    } else {
      return true;
    }
  }

  validateMPlace() {
    if (!this.meetingPlace || !this.meetingPlace.trim().length) {
      return false;
    } else {
      return true;
    }
  }

  validateMinCapacity() {
    if (!this.minCapacity || isNaN(this.minCapacity)) {
      return false;
    }
    return true;
  }

  validateMaxCapacity() {
    if (!this.maxCapacity || isNaN(this.maxCapacity)) {
      return false;
    }
    return true;
  }

  validateTransType() {
    if (!this.transType || !this.transType.trim().length) {
      return false;
    }
    return true;
  }

  validateForm() {
    let isFormValid = this.validateName() &&
      this.validateMPlace() &&
      this.validateMaxCapacity() &&
      this.validateMinCapacity() &&
      this.validateDuration() &&
      //this.validateTransType() &&
      this.validateMeetingTime() &&
      this.validateStartTime() &&
      this.validateDate();
    return isFormValid;
  }

  getMediaFile() {
    let mediaList = [];
    for (let i = 0; i < this.mediaFileList.length; i++) {
      // if (this.mediaFileList[i].url) {
      let obj = Object.assign({}, JSON.parse(JSON.stringify(this.mediaFileList[i])));
      delete obj["index"];
      mediaList.push(obj);
      // }
    }
    return mediaList;
  }
  saveExcursion(form) {
    this.formSubmitted = true;
    if (this.validateForm()) {
      let excursionData = {};
      if(this.selectedActivity=='Easy')
        {
          excursionData["activityLevel"]='1'
        }
        else if(this.selectedActivity=='Moderate'){
          excursionData["activityLevel"]='2'
        }
        else{
          excursionData["activityLevel"]='3'
        }

      excursionData = {

        "name": this.excursionName,
        "vendor": this.selectedVendor,
        "country": this.selectedCountry,
        "port": this.selectedPort,
        "startTime": this.startTime ? this.startTime : null,
        "duration": (this.durationHrs ? this.durationHrs * 60 : 0) + (this.durationMins ? this.durationMins : 0),
        "meetingTime": this.meetingTime ? this.meetingTime : null,
        "meetingPlace": this.meetingPlace,
        "category": this.selectedCategory,
        // "activityLevel": this.selectedActivity,
        
        "cruiseType": this.cruiseTypeEnum[this.selectedType],
        "capacity": {
          "min": this.minCapacity,
          "max": this.maxCapacity
        },
        "prices": this.getPriceData(),
        "transportation": {
          "type": this.transType,
          "comment": this.transCom,
          "isInsuranceRequired": this.isInsuranceReq,
          "isACAvailable": this.isAC,
          "isCovered": this.isCovered
        },
        "activeDuration": {
          "startDate": +this.startDate,
          "endDate": +this.endDate
        },
        "rules": {
        },
        "publishOn": {
          web: this.isPublishOnWeb,
          app: this.isPublishOnApp,
          chat: this.isPublishOnChatbot,
          kisok: this.isPublishOnKisok
        },
     
        "content": {
          "language": this.language,
          "description": this.description,
          "instructions": this.instructions,
          "inclusions": this.inclusions,
          "exclusions": this.exclusions,
          "media": this.getMediaFile()
        }
      }

      let rulesData = {};

      if (this.isWeightRule) {
        rulesData["weight"] = {
          "type": this.typeEnum[this.weightTypeSelected],
          "value": this.weightVal
        };
      }
      if (this.isHeightRule) {
        rulesData["height"] = {
          "type": this.typeEnum[this.heightTypeSelected],
          "value": this.heightVal
        };
      }
      if (this.isOverWeightRule) {
        rulesData["overweightCharge"] = {
          // "currency": this.selectedCurrency,
          "type":this.overWeightChargesType === "%" ?"PERCENT" : "FIXED",
          "charge": this.overweightVal
        }
      }
      if (this.isAgeRule) {
        rulesData["age"] = {
          "type": this.typeEnum[this.ageTypeSelected],
          "value": this.ageVal
        }
      }
      if (this.isCancellationRule) {
        rulesData["cancellationCharge"] = {
          "type": this.cancellationType === "%" ? "PERCENT" : "FIXED",
          "value": this.cancellationCharge
        }
      }
      rulesData["applyPromotion"] = this.isNoPromotionRule;
      rulesData["oneTicketPerPerson"] = this.isOneTicketPerPersonRule;
      rulesData["allowEmbarkingGuests"] = this.embarkingGuest;
      rulesData["allowDisembarkingGuests"] = this.disembarkingGuest;
      rulesData["allowInTransitGuests"] = this.intransitGuest;
      rulesData["daysOfWeek"] = this.daysOfWeek;
      // rulesData["status"]=this.status;
      switch(this.status){
       
        case 'ACTIVE':
        excursionData["status"]="ACTIVE";
        break;
        case 'INACTIVE':
        excursionData["status"]="INACTIVE";
        break;
      }

      excursionData["rules"] = rulesData;


      if (this.isEdit) {
        excursionData["id"] = this.id;
      }

      console.log(excursionData);
    
      this.callSaveExcursion(excursionData);

    }
  }

  callSaveExcursion(excursionData) {
    let url = this.isEdit ? "editExcursion" : "saveExcursion";

    this.httpService.call(url, excursionData).subscribe((data) => {
      this.alert = {
        type: "success",
        title: "Success",
        message: "Excursion saved successfully"
      };
      this.isEdit=false;
      this.notify.emit(this.alert);
      this.resetTab.emit();
    }, (error) => {
      this.alert = {
        type: "error",
        title: "Failed",
        message: "Some error occured"
      };
      this.notify.emit(this.alert);
    });
  }

  getPriceData() {
    let priceArr = [];
    for (let i = 0; i < this.priceRows.length; i++) {
      let obj = Object.assign({}, JSON.parse(JSON.stringify(this.priceRows[i])));
      obj.validFrom = +new Date(obj.priceValidFrom);
      obj.validTo = +new Date(obj.priceValidTo);
      delete obj["no"];
      priceArr.push(obj);
    }
    return priceArr;
  }

}