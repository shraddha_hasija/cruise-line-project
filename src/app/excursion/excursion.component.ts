import { Component, OnInit, ViewChild } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap';
import { HTTPService } from '../shared/services/http.service';


@Component({
  selector: 'app-excursion',
  templateUrl: './excursion.component.html',
  styleUrls: ['./excursion.component.css']
})
export class ExcursionComponent implements OnInit {

  @ViewChild('excursionTabs') excursionTabs: TabsetComponent;

  editData = {};
  listTabActive = false;
  isShowAlert: boolean = false;
  isRefreshList: boolean = false;
  alert = {
    message: '',
    type: "",
    title: ""
  };
  isAuthenticated: boolean = false;

  constructor(private httpService: HTTPService) { }

  ngOnInit() {
    this.getAuthToken();
  }

  changeTab(tabId) {
    this.excursionTabs.tabs[tabId].active = true;
    this.excursionTabs.tabs[tabId].heading = "Edit Excursion";
  }

  resetExcursion(tabId) {
    if (tabId == 1) {
      this.excursionTabs.tabs[tabId].heading = "New Excursion";
    }
    this.excursionTabs.tabs[tabId].active = true;
  }

  editExcursion(data) {
    this.changeTab(1);
    this.editData = data;
  }

  resetTab() {
    this.resetExcursion(0);
    // this.selectTab(1)
   
  }

  selectTab(tab) {
    if (tab === 1) {
      this.isRefreshList = true;
      this.listTabActive = true;
    } else {
      this.isRefreshList = false;
      this.listTabActive = false;
    }
  }

  showNotification(msgObj) {
    this.isShowAlert = true;
    this.alert = msgObj;

    setTimeout(() => {
      this.isShowAlert = false;
    }, 3000);
  }

  getAuthToken() {
    let req = {
      "username": "user",
      "password": "user"
    };
    this.httpService.call("authenticate", req).subscribe((data: any) => {
      console.log(data);
      this.isAuthenticated = true;
      sessionStorage.setItem("authToken", "Bearer " + data.id_token);
    });
  }
}
