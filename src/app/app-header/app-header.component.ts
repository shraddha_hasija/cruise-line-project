import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent implements OnInit {

  menuList: any = {
    "Dashboard": ["Reports"],
    "On-board Services": ["Manage Excursion", "Book Excursion","Excursion Bookings", "Spa", "On-board Events", "Discounts", "Loyalty Management"],
    "Crew Apps": ["Check-in", "Gangway Security", "Room Maintenance & Housekeeping", "Time & Attendance"],
    "Customer Services": ["Guest Management", 'Customer Announcements', "Customer Feedbacks", "Chat Bot Analytics"],
    "Administration": ["User Management", "Alerts/Notifications"]
  };
  
  subMenuList = {
    "Manage Excursion": ["Manage","Book"]
    // ,"Dining": ["test","Book"]

  };
  menuRoute: any = {
    "Reports": "dashboard",
    "Dining": 'dining/list',
    "Spa": "spa",

    // "Manage": "excursion/list",
    // "Book": "excursion/book/new"
    
    "Manage Excursion": "excursion/list",
    "Book Excursion": "excursion/book/new",
    "Excursion Bookings": "excursion/book/list",
    "Discounts":"discount/list",
  }
  
  menuListArr = [];


  constructor() { }

  ngOnInit() {
    this.menuListArr = Object.keys(this.menuList);
  }

}
